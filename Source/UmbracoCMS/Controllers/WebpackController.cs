﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using UmbracoCMS.Models;

namespace UmbracoCMS.Controllers
{
    public class WebpackController : SurfaceController
    {
        public ActionResult Import(WebpackModel form)
        {
            return RedirectToCurrentUmbracoUrl();
        }

    }
}
