﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Webpack.Domain.Analytics;
using Webpack.Domain.Analytics.Crawler;
using Webpack.Domain.Analytics.Crawler.Handlers;
using Webpack.Domain.Model;
using Webpack.Domain.Model.Entities;
using Webpack.Domain.Analytics.ModelAnalysis;

namespace Webpack.Client.CL
{
    class Program
    {
        static void Main(string[] args)
        {
            var crawlerCfg = new CrawlerConfiguration
            {
                Uri = new Uri("http://setkani.prvakoviny.muni.cz/"),
                CountLimit = 500,
                IgnoredPrefixes = new[] { "/umbraco/" },
                IgnoredPaths = new[] { "/cs/systemove/rss" }
            };
            var directory = "webpack";
            var imageHandler = new ImageHandler(directory, null);
            var scriptHandler = new ScriptHandler(directory, imageHandler);
            var loader = new HtmlAgilityPackLoader(scriptHandler);
            var crawler = new DefaultCrawler(crawlerCfg, loader);

            var analyticsRunner = new AnalyticsRunner(crawler, new PrvakovinyAnalyzer());

            var site = analyticsRunner.Run();
            site.Name = "Setkavani";

            var serializer = new SiteSerializer();
            var desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //desktopPath += Path.DirectorySeparatorChar + "export" + Path.DirectorySeparatorChar;
            serializer.Serialize(site, desktopPath);

            string extractedPath;
            var desSite = serializer.Deserialize(desktopPath + Path.DirectorySeparatorChar + "package.wbp", out extractedPath);
        }
    }

    public class PrvakovinyAnalyzer : ModelAnalyzer
    {
        public override void Build()
        {
            AddLanguageVersion(Allways);

            var landing = new PageModel("landing")
            {
                Meets = Rule(c => c.Path == "/" || c.Path == string.Empty),
            };

            var articles = new PageModel("article")
            {
                Organizer = new HierarchyOrganizer(),
                Meets = Rule(c => c.Url.SegmentPredicate(1, s => s == "setkani"))
            };

            var info = new PageModel("info")
            {
                Meets = Allways
            };

            Hook.AddChildren(landing, articles, info);
        }
    }
}
