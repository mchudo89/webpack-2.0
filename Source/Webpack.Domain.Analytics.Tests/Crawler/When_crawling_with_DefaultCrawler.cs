﻿// <copyright file="When_crawling_with_DefaultCrawler.cs" company="ÚVT MU">
//     Copyright (c) ÚVT MU. All rights reserved.
// </copyright>
// <author>Matej Chudo</author>
namespace Webpack.Domain.Analytics.Tests.Crawler
{
    using System;
    using FakeItEasy;
    using Webpack.Domain.Analytics.Crawler;
    using Webpack.Domain.Model.Entities;
    using Xunit;
    
    /// <summary>
    /// Unit tests of <seealso cref="DefaultCrawler" />, method <seealso cref="DefaultCrawler.Crawl"/>
    /// </summary>
    public class When_crawling_with_DefaultCrawler
    {
        /// <summary>
        /// Test whether countLimit gets exceeded or not
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_respect_countLimit_1()
        {
            // Arange
            int i = 0;
            var loader = A.Fake<HtmlAgilityPackLoader>(options => options.WithArgumentsForConstructor(new object[] {null}));
            A.CallTo(() => loader.Load(A<Uri>.Ignored, A<Uri>.Ignored))
                .ReturnsLazily(() => new RawPage((i++).GetTestUri(), "unit-test", string.Empty, new Uri[] { null, null, null }));

            var configuration = new CrawlerConfiguration
            {
                Uri = new Uri("http://www.muni.cz"),
                CountLimit = 0,
                DepthLimit = 0,
            };
            var crawler = new DefaultCrawler(configuration, loader);

            // Act
            var pages = crawler.Crawl();

            // Assert
            Assert.Empty(pages);
        }

        /// <summary>
        /// Test whether countLimit gets exceeded or not
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_respect_countLimit_2()
        {
            // Arange
            int i = 0;
            var loader = A.Fake<HtmlAgilityPackLoader>(options => options.WithArgumentsForConstructor(new object[] { null }));
            A.CallTo(() => loader.Load(A<Uri>.Ignored, A<Uri>.Ignored))
                .ReturnsLazily(() => new RawPage((i++).GetTestUri(), "unit-test", string.Empty, 
                    new Uri[] { new Uri("http://www.muni.cz") }));

            var configuration = new CrawlerConfiguration
            {
                Uri = new Uri("http://www.muni.cz"),
                CountLimit = 1,
                DepthLimit = 0,
            };
            var crawler = new DefaultCrawler(configuration, loader);

            // Act
            var pages = crawler.Crawl();

            // Assert
            Assert.NotEmpty(pages);
            Assert.Equal(1, pages.Count);
        }

        /// <summary>
        /// Test whether depthLimit gets exceeded or not
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_respect_depthLimit_1()
        {
            // Arange
            int i = 0;
            var loader = A.Fake<HtmlAgilityPackLoader>(options => options.WithArgumentsForConstructor(new object[] { null })); ;
            A.CallTo(() => loader.Load(A<Uri>.Ignored, A<Uri>.Ignored))
                .ReturnsLazily(() => new RawPage(i++.GetTestUri(), "unit-test", string.Empty, 
                    new Uri[] { new Uri("http://www.muni.cz") }));

            var configuration = new CrawlerConfiguration
            {
                Uri = new Uri("http://www.muni.cz"),
                CountLimit = 10,
                DepthLimit = 0,
            };
            var crawler = new DefaultCrawler(configuration, loader);

            // Act
            var pages = crawler.Crawl();

            // Assert
            Assert.NotEmpty(pages);
            Assert.Equal(1, pages.Count);
        }

        /// <summary>
        /// Test whether depthLimit gets exceeded or not
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_respect_depthLimit_2()
        {
            // Arange
            int i = 0;
            var loader = A.Fake<HtmlAgilityPackLoader>(options => options.WithArgumentsForConstructor(new object[] { null })); ;
            A.CallTo(() => loader.Load(A<Uri>.Ignored, A<Uri>.Ignored))
                .ReturnsLazily(() => new RawPage((++i).GetTestUri(), "unit-test" + i, string.Empty, 
                    new Uri[] { new Uri("http://www.muni.cz/?i=" + i.ToString()) }));

            var configuration = new CrawlerConfiguration {
                Uri = new Uri("http://www.muni.cz"),
                CountLimit = 10,
                DepthLimit = 1,
            };
            var crawler = new DefaultCrawler(configuration, loader);

            // Act
            var pages = crawler.Crawl();

            // Assert
            Assert.NotEmpty(pages);
            Assert.True(pages.Count > 1);
        }
    }
}
