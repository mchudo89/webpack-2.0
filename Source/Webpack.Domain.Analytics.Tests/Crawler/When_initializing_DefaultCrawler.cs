﻿// <copyright file="When_initializing_DefaultCrawler.cs" company="ÚVT MU">
//     Copyright (c) ÚVT MU. All rights reserved.
// </copyright>
// <author>Matej Chudo</author>
namespace Webpack.Domain.Analytics.Tests.Crawler
{
    using System;
    using Webpack.Domain.Analytics.Crawler;
    using Xunit;

    /// <summary>
    /// Unit tests of <seealso cref="DefaultCrawler"/> constructor.
    /// </summary>
    public class When_initializing_DefaultCrawler
    {
        /// <summary>
        /// Testing if constructor throws ArgumentNullException when configuration is <c>null</c>.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_throw_If_configuration_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => 
            {
                new DefaultCrawler(null, null);
            });
        }

        /// <summary>
        /// Testing if constructor initializes the crawler when a proper configuration is passed.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_create_instance()
        {
            // Arange
            var loader = new HtmlAgilityPackLoader();
            var configuration = new CrawlerConfiguration
            {
                Uri = new Uri("http://www.muni.cz"),
                CountLimit = 0,
                DepthLimit = 0,
            };
            var crawler = new DefaultCrawler(configuration, loader);

            // Assert
            Assert.NotNull(crawler);
            Assert.IsType<DefaultCrawler>(crawler);
            Assert.IsAssignableFrom<ICrawler>(crawler);
        }

        /// <summary>
        /// Testing if constructor initializes the crawler and sets <c>BaseUri</c> property.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_set_BaseUri()
        {
            // Arange
            var loader = new HtmlAgilityPackLoader();
            var uri = new Uri("http://www.muni.cz");
            var configuration = new CrawlerConfiguration
            {
                Uri = uri,
                CountLimit = 0,
                DepthLimit = 0,
            };
            
            // Act
            var crawler = new DefaultCrawler(configuration, loader);

            // Assert
            Assert.Equal(crawler.BaseUri, uri);
        }
    }
}
