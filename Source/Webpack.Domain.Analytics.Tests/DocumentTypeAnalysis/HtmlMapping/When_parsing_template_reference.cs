﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using Webpack.Domain.Analytics.DocumentTypeAnalysis.HtmlMapping;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.HtmlMapping
{
    public class When_parsing_template_reference
    {
        [Fact(Timeout=1000)]
        public void Should_throw_if_null()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            
            // Act + Assert
            Assert.Throws<ArgumentNullException>(() => propertyFactory.ParseTemplateReference((HtmlNode)null));
            Assert.Throws<ArgumentNullException>(() => propertyFactory.ParseTemplateReference((string)null));
        }

        [Fact(Timeout=1000)]
        public void Should_return_a_property_definition_instance_on_success()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var property = propertyFactory.GetNew();

            // Act
            var result = propertyFactory.ParseTemplateReference(property.TemplateReference);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(property.Name, result.Name);
            Assert.Equal(property.TemplateReference, result.TemplateReference);
        }

        [Fact(Timeout=1000)]
        public void Should_return_null_on_failture()
        {
            // Arange 
            var propertyFactory = new PropertyFactory();
            
            // Act
            var result = propertyFactory.ParseTemplateReference("xy");

            // Assert
            Assert.Null(result);
        }
    }
}
