﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using Webpack.Domain.Analytics.DocumentTypeAnalysis.HtmlMapping;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.HtmlMapping
{
    public class When_searching_for_properties
    {
        [Fact(Timeout=1000)]
        public void Should_not_create_property_for_same_html()
        {
            // Arrange
            var html = "<html><head><title>Title</title></head><body>Body</body></html>";
            var skeleton = ParseSkeleton(html);
            var builder = GetBuilder();

            // Act
            var result = builder.Build(skeleton, html);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Properties);
            Assert.Equal(html, result.Template.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_throw_if_skeleton_is_not_subset_of_page_1()
        {
            // Arrange
            var skeleton = ParseSkeleton("Hello");
            var html = "Not Hello";

            var builder = GetBuilder();

            // Act + Assert
            Assert.Throws<InvalidOperationException>(() => builder.Build(skeleton, html));
        }

        [Fact(Timeout=1000)]
        public void Should_throw_if_skeleton_is_not_subset_of_page_2()
        {
            // Arrange
            var skeleton = ParseSkeleton("<html>Hello</html>");
            var html = "<html>Not Hello</html>";

            var builder = GetBuilder();

            // Act + Assert
            Assert.Throws<InvalidOperationException>(() => builder.Build(skeleton, html));
        }

        [Fact(Timeout=1000)]
        public void Should_create_property_for_simple_subset_html_tree()
        {
            // Arrange
            var skeleton = ParseSkeleton("<html><head><title>Title</title></head><body></body></html>");
            var html = "<html><head><title>Title</title></head><body>Body</body></html>";

            var builder = GetBuilder();

            // Act
            var result = builder.Build(skeleton, html);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result.Properties);
            var property = result.Properties.Single();
            var expected = string.Format("<html><head><title>Title</title></head><body>{0}</body></html>", property.TemplateReference);
            Assert.Equal(expected, result.Template.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_create_property_for_complex_subset_html_tree()
        {
            // Arrange
            var skeleton = ParseSkeleton(@"<html><head><title>Title</title></head><body><div class=""menu""></div></body></html>");
            var html = @"<html><head><title>Title</title></head><body>Body<div class=""menu""><ul><li>Item 1</li><li>Item 2</li></ul></div><article></article><footer>Footer</footer></body></html>";

            var builder = GetBuilder();
            
            // Act
            var result = builder.Build(skeleton, html);

            // Assert
            Assert.Equal(3, result.Properties.Count);
            for (int i = 0; i < 3; i++)
			{
                Assert.Contains(result.Properties[i].TemplateReference, result.Template.WriteTo());
			}
        }

        [Fact(Timeout=1000)]
        public void Should_reuse_property_name()
        {
            // Arrange
            var propertyFactory = new PropertyFactory();
            var property = propertyFactory.Get(123456);
            var skeletonHtml = string.Format("<html><head><title>Title</title></head><body>{0}</body></html>", property.TemplateReference);
            var skeleton = ParseSkeleton(skeletonHtml);
            var html = "<html><head><title>Title</title></head><body>Body</body></html>";

            var builder = GetBuilder();

            // Act
            var result = builder.Build(skeleton, html);

            // Assert
            Assert.Equal(skeletonHtml, result.Template.WriteTo());
            Assert.Single(result.Properties);
            Assert.Single(result.Properties, p => p.Number == property.Number);
            Assert.Equal("Body", result.Properties.Single().Value);
        }

        [Fact(Timeout=1000)]
        public void Should_return_empty_property()
        {
            // Arrange
            var propertyFactory = new PropertyFactory();
            var property = propertyFactory.Get(123456);
            var skeletonHtml = string.Format("<html><head><title>Title</title></head><body>{0}</body></html>", property.TemplateReference);
            var skeleton = ParseSkeleton(skeletonHtml);
            var html = "<html><head><title>Title</title></head><body></body></html>";

            var builder = GetBuilder();

            // Act
            var result = builder.Build(skeleton, html);

            // Assert
            Assert.Equal(skeletonHtml, result.Template.WriteTo());
            Assert.Single(result.Properties);
            Assert.Single(result.Properties, p => p.Number == property.Number);
            Assert.Equal(string.Empty, result.Properties.Single().Value);
        }

        [Fact(Timeout=1000)]
        public void Should_expand_property()
        {
            // Arrange
            var propertyFactory = new PropertyFactory();
            var property = propertyFactory.Get(123456);
            var skeletonHtml = string.Format("<html><head><title>Title</title></head><body>{0}</body></html>", property.TemplateReference);
            var skeleton = ParseSkeleton(skeletonHtml);
            var html = "<html><head><title>Title</title></head><body><h1>Hello world</h1></body></html>";

            var builder = GetBuilder();

            // Act
            var result = builder.Build(skeleton, html);

            // Assert
            Assert.Equal(skeletonHtml, result.Template.WriteTo());
            Assert.Single(result.Properties);
            Assert.Single(result.Properties, p => p.Number == property.Number);
            Assert.Equal("<h1>Hello world</h1>", result.Properties.Single().Value);
        }

        private Builder GetBuilder()
        {
            return new Builder(new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyFactory());
        }

        private static HtmlNode ParseSkeleton(string text)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(text);
            return doc.DocumentNode;
        }
    }
}
