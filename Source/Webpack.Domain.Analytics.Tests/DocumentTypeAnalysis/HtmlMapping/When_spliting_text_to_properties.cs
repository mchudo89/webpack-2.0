﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using Webpack.Domain.Analytics.DocumentTypeAnalysis.HtmlMapping;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.HtmlMapping
{
    public class When_spliting_text_to_properties
    {
        [Fact(Timeout=1000)]
        public void Should_throw_if_text_null_or_empty()
        {
            var propertyFactory = new PropertyFactory();

            // Act + Assert
            Assert.Throws<ArgumentNullException>(() => propertyFactory.SplitText(null));
        }

        [Fact(Timeout=1000)]
        public void Should_return_1_if_no_properties_present()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            string text = "A lovely text paragraph with no properties.";

            // Act
            var result = propertyFactory.SplitText(text);

            // Assert
            Assert.Single(result, text);
        }

        [Fact(Timeout=1000)]
        public void Should_return_1_if_text_is_only_property()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var property = propertyFactory.GetNew();
            string text = property.TemplateReference;

            // Act
            var result = propertyFactory.SplitText(text);

            // Assert
            Assert.Single(result, property.TemplateReference);
        }

        [Fact(Timeout=1000)]
        public void Should_return_2_if_text_is_only_2_succeeding_properties()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var property1 = propertyFactory.GetNew();
            var property2 = propertyFactory.GetNew(); ;
            string text = property1.TemplateReference + property2.TemplateReference;

            // Act
            var result = propertyFactory.SplitText(text);

            // Assert
            Assert.Equal(new[] { property1.TemplateReference, property2.TemplateReference }, result);
        }

        [Fact(Timeout=1000)]
        public void Should_return_2_if_text_is_1_property_followed_by_some_text()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var property1 = propertyFactory.GetNew();
            string text = property1.TemplateReference + "\r\n";

            // Act
            var result = propertyFactory.SplitText(text);

            // Assert
            Assert.Equal(new[] { property1.TemplateReference, "\r\n" }, result);
        }

        [Fact(Timeout=1000)]
        public void Should_split_text_and_properties()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var property1 = propertyFactory.GetNew();
            var property2 = propertyFactory.GetNew();
            var property3 = propertyFactory.GetNew();
            string text = "\r\n" + property1.TemplateReference + property2.TemplateReference + "\r\n" + property3.TemplateReference + "no white space";

            // Act
            var result = propertyFactory.SplitText(text);

            // Assert
            Assert.Equal(new[] { "\r\n", property1.TemplateReference, property2.TemplateReference, "\r\n", property3.TemplateReference, "no white space" }, result);
        }
    }
}
