﻿namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.HtmlNodeComparing
{
    using HtmlAgilityPack;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Webpack.Domain.Analytics.DocumentTypeAnalysis;
    using Webpack.Domain.Analytics.DocumentTypeAnalysis.HtmlNodeComparing;
    using Xunit;

    public class When_initializing_MappedHtmlNode
    {
        [Fact]
        public void Root_should_have_level_0()
        {
            // Arrange + Act
            var node1 = new MappedHtmlNodeToProperty(null, "test", "test", new HtmlNode[0]);
            var node2 = new MappedHtmlNodeToReferenceNode(null, HtmlNode.CreateNode("test"), HtmlNode.CreateNode("test"), EqualityComparer<HtmlNode>.Default, new PropertyNameState());

            // Assert
            Assert.Equal(0, node1.Level);
            Assert.Equal(0, node2.Level);
        }

        [Fact]
        public void Child_should_have_level_higher_by_1_to_parent()
        {
            // Arrange + Act
            var root = new MappedHtmlNodeToReferenceNode(null, HtmlNode.CreateNode("test"), HtmlNode.CreateNode("test"), EqualityComparer<HtmlNode>.Default, new PropertyNameState());
            var node1 = new MappedHtmlNodeToProperty(root, "test", "test", new HtmlNode[0]);
            var node2 = new MappedHtmlNodeToReferenceNode(root, HtmlNode.CreateNode("test"), HtmlNode.CreateNode("test"), EqualityComparer<HtmlNode>.Default, new PropertyNameState());

            // Assert
            Assert.Equal(1, node1.Level);
            Assert.Equal(1, node2.Level);
        }

        [Fact]
        public void Should_map_same_html_to_MappedHtmlNodeReferenceNode()
        {
            // Arrange
            var html = "<html><head><title>Title</title></head><body>Body</body></html>";
            var node = HtmlNode.CreateNode(html);

            // Act
            var map = new MappedHtmlNodeToReferenceNode(null, node, node, new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyNameState());

            // Assert
            foreach (var mappedNode in map)
            {
                Assert.IsType<MappedHtmlNodeToReferenceNode>(mappedNode);
                Assert.Equal(mappedNode.MappedNodeText, mappedNode.ReferenceNodeText);
            }
        }

        [Fact]
        public void Should_create_property_for_simple_subset_html_tree()
        {
            // Arrange
            var skeletonHtml = "<html><head><title>Title</title></head><body></body></html>";
            var skeleton = HtmlNode.CreateNode(skeletonHtml);

            var html = "<html><head><title>Title</title></head><body>Body</body></html>";
            var node = HtmlNode.CreateNode(html);

            // Act
            var map = new MappedHtmlNodeToReferenceNode(null, node, skeleton, new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyNameState());

            // Assert
            var hasProperty = false;
            foreach (var mappedNode in map)
            {
                if (mappedNode.GetType() == typeof(MappedHtmlNodeToProperty))
                {
                    hasProperty = true;
                }
            }

            Assert.True(hasProperty);
        }

        [Fact]
        public void Should_create_property_for_complex_subset_html_tree()
        {
            // Arrange
            var skeletonHtml = @"<html><head><title>Title</title></head><body><div class=""menu""></div></body></html>";
            var skeleton = HtmlNode.CreateNode(skeletonHtml);

            var html = @"<html><head><title>Title</title></head><body>Body<div class=""menu""><ul><li>Item 1</li><li>Item 2</li></ul></div><article></article><footer>Footer</footer></body></html>";
            var node = HtmlNode.CreateNode(html);

            // Act
            var map = new MappedHtmlNodeToReferenceNode(null, node, skeleton, new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyNameState());

            // Assert
            var properties = 0;
            foreach (var mappedNode in map)
            {
                if (mappedNode.GetType() == typeof(MappedHtmlNodeToProperty))
                {
                    properties++;
                }
            }

            Assert.Equal(3, properties);
        }

        [Fact]
        public void Should_reuse_property_name()
        {
            // Arrange
            var skeletonHtml = "<html><head><title>Title</title></head><body>@Model.Property12345</body></html>";
            var skeleton = HtmlNode.CreateNode(skeletonHtml);

            var html = "<html><head><title>Title</title></head><body>Body</body></html>";
            var node = HtmlNode.CreateNode(html);

            // Act
            var map = new MappedHtmlNodeToReferenceNode(null, node, skeleton, new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyNameState());

            // Assert
            Assert.Equal("@Model.Property12345", map.Children[1].Children[0].ReferenceNodeText);
            Assert.Equal("Body", map.Children[1].Children[0].MappedNodeText);
        }

        [Fact]
        public void Should_expand_property()
        {
               // Arrange
            var skeletonHtml = "<html><head><title>Title</title></head><body>@Model.Property45</body></html>";
            var skeleton = HtmlNode.CreateNode(skeletonHtml);

            var html = "<html><head><title>Title</title></head><body>Body<h1>Hello world!</h1></body></html>";
            var node = HtmlNode.CreateNode(html);

            // Act
            var map = new MappedHtmlNodeToReferenceNode(null, node, skeleton, new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyNameState());

            // Assert
            Assert.Equal("@Model.Property45", map.Children[1].Children[0].ReferenceNodeText);
            Assert.Equal("Body<h1>Hello world!</h1>", map.Children[1].Children[0].MappedNodeText);
        }

        [Fact]
        public void Should_expand_property_2()
        {
            // Arrange
            var skeletonHtml = "<html><head><title>Title</title></head><body><h1>Title</h1>@Model.Property75<footer>Footer</footer></body></html>";
            var skeleton = HtmlNode.CreateNode(skeletonHtml);

            var html = "<html><head><title>Title</title></head><body><h1>Title</h1><div></div><footer>Footer</footer></body></html>";
            var node = HtmlNode.CreateNode(html);

            // Act
            var map = new MappedHtmlNodeToReferenceNode(null, node, skeleton, new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyNameState());

            // Assert
            Assert.Equal("@Model.Property75", map.Children[1].Children[1].ReferenceNodeText);
            Assert.Equal("<div></div>", map.Children[1].Children[1].MappedNodeText);
        }
    }
}
