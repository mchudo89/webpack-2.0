﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Logic;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class UrlNodeEqualityComparer : IEqualityComparer<UrlNode>
    {
        public bool Equals(UrlNode x, UrlNode y)
        {
            if (x == null && y == null)
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.Path == y.Path;
        }

        public int GetHashCode(UrlNode obj)
        {
            if (obj == null)
            {
                return 0;
            }
            if (obj.Path == null)
	        {
		        return obj.GetType().GetHashCode();
	        }
            return obj.Path.GetHashCode();
        }
    }
}
