﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using Webpack.Domain.Analytics.DocumentTypeAnalysis.HtmlMapping;
using Webpack.Domain.Analytics.Extensions;
using Webpack.Domain.Analytics.HierarchyAnalysis;
using Webpack.Domain.Model.Entities;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class When_analysing_menu
    {
        [Fact(Timeout=1000)]
        public void Should_return_a_copy_one_menu_is_passed()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var propertyData = new Dictionary<Page, Dictionary<string, string>> 
            {
                {
                    new Page(),
                    new Dictionary<string, string> 
                    { 
                        {
                            "menu11",
                            @"<ul>
                                <li><a href=""/1""></a></li>
                                <li><a href=""/2""></a></li>
                                <li><a href=""/3""></a></li>
                                <li><a href=""/4""></a></li>
                            </ul>"
                        } 
                    }
                }
            };

            // Act
            var result = menuHierarchyFinder.FindHierarchy(propertyData);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Children);
            Assert.Equal(4, result.Children.Count);
            for (int i = 0; i < result.Children.Count; i++)
            {
                Assert.Equal("/" + (i + 1).ToString(), result.Children[i].Path);
            }
        }

        [Fact(Timeout=1000)]
        public void Should_merge_two_menus_in_one()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var propertyData = new Dictionary<Page, Dictionary<string, string>> 
            {
                {
                    new Page(),
                    new Dictionary<string, string> 
                    { 
                        {
                            "menu1",
                            @"<ul>
                                <li><a href=""/1""></a></li>
                                <li><a href=""/2""></a></li>
                                <li><a href=""/3""></a></li>
                                <li><a href=""/4""></a></li>
                            </ul>"
                        },
                        {
                            "menu2",
                            @"<ul>
                                <li><a href=""/1""></a></li>
                                <li><a href=""/2""></a></li>
                                <li><a href=""/3""></a></li>
                                <li><a href=""/5""></a></li>
                            </ul>"
                        }
                    }
                }
            };

            // Act
            var result = menuHierarchyFinder.FindHierarchy(propertyData);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Children);
            Assert.Equal(5, result.Children.Count);
            for (int i = 0; i < result.Children.Count; i++)
            {
                Assert.Equal("/" + (i + 1).ToString(), result.Children[i].Path);
            }
        }

        [Fact(Timeout=1000)]
        public void Should_merge_two_deeper_menu_nodes_in_one()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var propertyData = new Dictionary<Page, Dictionary<string, string>> 
            {
                {
                    new Page(),
                    new Dictionary<string, string> 
                    { 
                        { 
                            "menu1",
                            @"<ul><li><a href=""/0""></a>
                                <ul>
                                    <li><a href=""/1""></a></li>
                                    <li><a href=""/2""></a></li>
                                    <li><a href=""/3""></a></li>
                                    <li><a href=""/4""></a></li>
                                </ul>
                            </li></ul>"
                        },
                        {
                            "menu2",
                            @"<ul><li><a href=""/0""></a>
                            <ul>
                                <li><a href=""/5""></a></li>
                                <li><a href=""/6""></a></li>
                                <li><a href=""/7""></a></li>
                                <li><a href=""/8""></a></li>
                            </ul>
                        </li></ul>"
                        }
                    }
                }
            };

            // Act
            var result = menuHierarchyFinder.FindHierarchy(propertyData);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result.Children);
            Assert.Equal(8, result.Children.Single().Children.Count);
            for (int i = 0; i < result.Children.Single().Children.Count; i++)
            {
                Assert.Equal("/" + (i + 1).ToString(), result.Children[0].Children[i].Path);
            }
        }

        [Fact(Timeout=1000)]
        public void Should_merge_three_deeper_menu_nodes_in_one()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var propertyData = new Dictionary<Page, Dictionary<string, string>> 
            {
                {
                    new Page(),
                    new Dictionary<string, string> 
                    { 
                        {
                            "menu1",
                            @"<ul><li><a href=""/0""></a>
                                <ul>
                                    <li><a href=""/1""></a></li>
                                    <li><a href=""/2""></a></li>
                                </ul>
                            </li></ul>"
                        },
                        {
                            "menu2",
                            @"<ul><li><a href=""/0""></a>
                                <ul>
                                    <li><a href=""/3""></a></li>
                                    <li><a href=""/4""></a></li>
                                    <li><a href=""/5""></a></li>
                                </ul>
                            </li></ul>"
                        },
                        {
                            "menu3",
                            @"<ul><li><a href=""/0""></a>
                            <ul>
                                <li><a href=""/6""></a></li>
                                <li><a href=""/7""></a></li>
                                <li><a href=""/8""></a></li>
                            </ul>
                        </li></ul>"
                        }
                    }
                }
            };

            // Act
            var result = menuHierarchyFinder.FindHierarchy(propertyData);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result.Children);
            Assert.Equal(8, result.Children.Single().Children.Count);
            for (int i = 0; i < result.Children.Single().Children.Count; i++)
            {
                Assert.Equal("/" + (i + 1).ToString(), result.Children[0].Children[i].Path);
            }
        }

        [Fact(Timeout=1000)]
        public void Should_merge_three_deeper_menu_nodes_into_multiple()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var propertyData = new Dictionary<Page, Dictionary<string, string>> 
            {
                {
                    new Page(),
                    new Dictionary<string, string> 
                    { 
                        {
                            "menu1",
                            @"<ul>
                                <li><a href=""/0""></a>
                                    <ul>
                                        <li><a href=""/0/0""></a></li>
                                        <li><a href=""/0/1""></a></li>
                                    </ul>
                                </li>
                                <li><a href=""/1""></a>
                                    <ul>
                                        <li><a href=""/1/0""></a></li>
                                        <li><a href=""/1/1""></a></li>
                                        <li><a href=""/1/2""></a></li>
                                    </ul>
                                </li>
                            </ul>"
                        },
                        {
                            "menu2",
                            @"<ul>
                                <li><a href=""/0""></a>
                                    <ul>
                                        <li><a href=""/0/0""></a></li>
                                        <li><a href=""/0/1""></a></li>
                                        <li><a href=""/0/2""></a></li>
                                    </ul>
                                </li>
                                <li><a href=""/1""></a>
                                    <ul>
                                        <li><a href=""/1/0""></a></li>
                                        <li><a href=""/1/2""></a></li>
                                    </ul>
                                </li>
                            </ul>"
                        },
                        {
                            "menu3",
                            @"<ul>
                                <li><a href=""/0""></a>
                                    <ul>
                                        <li><a href=""/0/0""></a></li>
                                        <li><a href=""/0/2""></a></li>
                                    </ul>
                                </li>
                                <li><a href=""/1""></a>
                                    <ul>
                                        <li><a href=""/1/1""></a></li>
                                    </ul>
                                </li>
                                <li><a href=""/2""></a>
                                    <ul>
                                        <li><a href=""/2/0""></a></li>
                                        <li><a href=""/2/1""></a></li>
                                        <li><a href=""/2/2""></a></li>
                                    </ul>
                                </li>
                            </ul>"
                        }
                    }
                }
            };

            // Act
            var result = menuHierarchyFinder.FindHierarchy(propertyData);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(3, result.Children.Count);
            for (int i = 0; i < result.Children.Count; i++)
            {
                var page = result.Children[i];
                Assert.Equal("/" + i.ToString(), result.Children[i].Path);
                for (int j = 0; j < page.Children.Count; j++)
                {
                    var subpage = page.Children[j];
                    Assert.Equal("/" + i.ToString() + "/" + j.ToString(), subpage.Path);
                }
            }
        }

        [Fact(Timeout=1000)]
        public void Should_merge_three_menu_3level_deep_nodes_into_multiple()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var propertyData = new Dictionary<Page, Dictionary<string, string>> 
            {
                {
                    new Page(),
                    new Dictionary<string, string> 
                    { 
                        {
                            "menu1",
                            @"<ul>
                                <li><a href=""/0""></a>
                                    <ul>
                                        <li><a href=""/0/0""></a>
                                            <ul>
                                                <li><a href=""/0/0/0""></a></li>
                                                <li><a href=""/0/0/1""></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=""/0/1""></a></li>
                                    </ul>
                                </li>
                                <li><a href=""/1""></a>
                                    <ul>
                                        <li><a href=""/1/0""></a>
                                            <ul>
                                                <li><a href=""/1/0/1""></a></li>
                                                <li><a href=""/1/0/2""></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=""/1/1""></a>
                                            <ul>
                                                <li><a href=""/1/1/0""></a></li>
                                                <li><a href=""/1/1/2""></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=""/1/2""></a>
                                            <ul>
                                                <li><a href=""/1/2/0""></a></li>
                                                <li><a href=""/1/2/1""></a></li>
                                                <li><a href=""/1/2/2""></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>"
                        },
                        {
                            "menu2",
                            @"<ul>
                                <li><a href=""/0""></a>
                                    <ul>
                                        <li><a href=""/0/0""></a>
                                            <ul>
                                                <li><a href=""/0/0/2""></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=""/0/1""></a>
                                            <ul>
                                                <li><a href=""/0/1/0""></a></li>
                                                <li><a href=""/0/1/1""></a></li>
                                                <li><a href=""/0/1/2""></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=""/0/2""></a>
                                            <ul>
                                                <li><a href=""/0/2/0""></a></li>
                                                <li><a href=""/0/2/1""></a></li>
                                                <li><a href=""/0/2/2""></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href=""/1""></a>
                                    <ul>
                                        <li><a href=""/1/0""></a>
                                            <ul>
                                                <li><a href=""/1/0/0""></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=""/1/1""></a>
                                            <ul>
                                                <li><a href=""/1/1/1""></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=""/1/2""></a></li>
                                    </ul>
                                </li>
                            </ul>"
                        },
                        {
                            "menu3",
                            @"<ul>
                                <li><a href=""/0""></a>
                                    <ul>
                                        <li><a href=""/0/0""></a></li>
                                        <li><a href=""/0/2""></a></li>
                                    </ul>
                                </li>
                                <li><a href=""/1""></a>
                                    <ul>
                                        <li><a href=""/1/1""></a></li>
                                    </ul>
                                </li>
                                <li><a href=""/2""></a>
                                    <ul>
                                        <li><a href=""/2/0""></a></li>
                                        <li><a href=""/2/1""></a></li>
                                        <li><a href=""/2/2""></a></li>
                                    </ul>
                                </li>
                            </ul>"
                        }
                    }
                }
            };

            // Act
            var result = menuHierarchyFinder.FindHierarchy(propertyData);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(3, result.Children.Count);
            for (int i = 0; i < result.Children.Count; i++)
            {
                Assert.Single(result.Children, n => n.Path == string.Format("/{0}", i));
                
                var page = result.Children.Single(n => n.Path == string.Format("/{0}", i));
                if (i == 2)
                {
                    continue;
                }
                Assert.Equal(3, page.Children.Count);
                for (int j = 0; j < page.Children.Count; j++)
                {
                    Assert.Single(page.Children, n => n.Path == string.Format("/{0}/{1}", i, j));

                    var subpage = page.Children.Single(n => n.Path == string.Format("/{0}/{1}", i, j));
                    Assert.Equal(3, subpage.Children.Count);
                    for (int k = 0; k < subpage.Children.Count; k++)
                    {
                        Assert.Single(subpage.Children, n => n.Path == string.Format("/{0}/{1}/{2}", i, j, k));

                        var subsubpage = subpage.Children.Single(n => n.Path == string.Format("/{0}/{1}/{2}", i, j, k));
                        Assert.Empty(subsubpage.Children);
                    }
                }   
            }
        }
    }
}
