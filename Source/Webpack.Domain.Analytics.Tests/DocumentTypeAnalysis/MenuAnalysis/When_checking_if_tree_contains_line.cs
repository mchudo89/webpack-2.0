﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Logic;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class When_checking_if_tree_contains_line
    {
        [Fact(Timeout=1000)]
        public void Should_throw_if_line_null()
        {
            // Arange
            var node = UrlNode.CreateRoot();

            // Act + Assert
            Assert.Throws<ArgumentNullException>(() => node.ContainsLine(null));
        }

        [Fact(Timeout=1000)]
        public void Should_allways_contain_empty_line()
        {
            // Arange
            var line = Enumerable.Empty<UrlNode>();
            var node1 = UrlNode.CreateRoot();
            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1").AppendChild("2");
            node2.AppendChild("3");

            // Act 
            var result1 = node1.ContainsLine(line);
            var result2 = node2.ContainsLine(line);
            
            // Assert
            Assert.True(result1);
            Assert.True(result2);
        }

        [Fact(Timeout=1000)]
        public void Should_contain_single_node_line_with_path_which_is_in_tree()
        {
            // Arange
            var nodeFactory = UrlNode.CreateRoot();

            var line1 = new List<UrlNode>() { nodeFactory.AppendChild("2") };
            var line2 = new List<UrlNode>() { nodeFactory.AppendChild("5") };
            var line3 = new List<UrlNode>() { nodeFactory.AppendChild("3") };
            var node1 = UrlNode.CreateRoot();
            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1").AppendChild("2");
            node2.AppendChild("3");

            // Act 
            var result1 = node1.ContainsLine(line1);
            var result2 = node1.ContainsLine(line2);
            var result3 = node1.ContainsLine(line3);
            var result4 = node2.ContainsLine(line1);
            var result5 = node2.ContainsLine(line2);
            var result6 = node2.ContainsLine(line3);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
            Assert.False(result3);
            Assert.True(result4);
            Assert.False(result5);
            Assert.True(result6);
        }

        [Fact(Timeout=1000)]
        public void Should_contain_multi_node_line_with_paths_which_are_in_tree()
        {
            // Arange
            var nodeFactory = UrlNode.CreateRoot();

            var line1 = new List<UrlNode>() { nodeFactory, nodeFactory.AppendChild("1"), nodeFactory.AppendChild("2") };
            var line2 = new List<UrlNode>() { nodeFactory.AppendChild("1"), nodeFactory.AppendChild("2") };
            var node1 = UrlNode.CreateRoot();
            node1.AppendChild("1").AppendChild("9");
            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1").AppendChild("2");
            node2.AppendChild("3");
            var node3 = UrlNode.CreateRoot();
            node3.AppendChild("17").AppendChild("1").AppendChild("2").AppendChild("5");
            node3.AppendChild("3");

            // Act 
            var result1 = node1.ContainsLine(line1);
            var result2 = node1.ContainsLine(line2);
            var result3 = node2.ContainsLine(line1);
            var result4 = node2.ContainsLine(line2);
            var result5 = node3.ContainsLine(line1);
            var result6 = node3.ContainsLine(line2);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
            Assert.True(result3);
            Assert.True(result4);
            Assert.False(result5);
            Assert.True(result6);
        }

        [Fact(Timeout=1000)]
        public void Should_contain_multi_node_line_with_paths_which_exceed_the_tree()
        {
            // Arange
            var nodeFactory = UrlNode.CreateRoot();

            var line1 = new List<UrlNode>() { nodeFactory, nodeFactory.AppendChild("1"), nodeFactory.AppendChild("2"), nodeFactory.AppendChild("3") };
            var line2 = new List<UrlNode>() { nodeFactory.AppendChild("1"), nodeFactory.AppendChild("2") };
            
            var node1 = UrlNode.CreateRoot();
            node1.AppendChild("1").AppendChild("9");
            
            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1").AppendChild("2").AppendChild("3");
            node2.AppendChild("4").AppendChild("5");
            
            var node3 = UrlNode.CreateRoot();
            node3.AppendChild("17").AppendChild("5").AppendChild("1");
            node3.AppendChild("3");

            // Act 
            var result1 = node1.ContainsLine(line1);
            var result2 = node1.ContainsLine(line2);
            var result3 = node2.ContainsLine(line1);
            var result4 = node2.ContainsLine(line2);
            var result5 = node3.ContainsLine(line1);
            var result6 = node3.ContainsLine(line2);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
            Assert.True(result3);
            Assert.True(result4);
            Assert.False(result5);
            Assert.True(result6);
        }

    }
}
