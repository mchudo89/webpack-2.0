﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using model = Webpack.Domain.Model.Logic;
using Xunit;
using Webpack.Domain.Model.Logic;
using Webpack.Domain.Analytics.HierarchyAnalysis;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class When_comparing_Url_trees
    {[
        Fact]
        public void Should_compare_null()
        {
            // Arange
            var comparer = new UrlTreeEqualityComparer();
            var node = model.UrlNode.CreateRoot();

            // Act + Assert
            Assert.True(comparer.Equals(null, null));
            Assert.False(comparer.Equals(null, node));
            Assert.False(comparer.Equals(node, null));
        }

        [Fact(Timeout=1000)]
        public void Should_compare_single_and_zero_node_trees()
        {
            // Arange
            var comparer = new UrlTreeEqualityComparer();
            var node1 = UrlNode.CreateRoot().AppendChild("1");
            var node2a = UrlNode.CreateRoot().AppendChild("2");
            var node2b = UrlNode.CreateRoot().AppendChild("2");

            // Act + Assert
            Assert.False(comparer.Equals(node1, node2a));
            Assert.False(comparer.Equals(node1, node2b));
            Assert.True(comparer.Equals(node2a, node2b));
        }

        [Fact(Timeout=1000)]
        public void Should_compare_2_level_trees()
        {
            // Arange
            var comparer = new UrlTreeEqualityComparer();
            var node1 = UrlNode.CreateRoot();
            node1.AppendChild("1");
            node1.AppendChild("2");

            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1");
            node2.AppendChild("2");

            var node3 = UrlNode.CreateRoot();
            node3.AppendChild("1");
            node3.AppendChild("2");
            node3.AppendChild("3");

            // Act + Assert
            Assert.True(comparer.Equals(node1, node2));
            Assert.False(comparer.Equals(node1, node3));
            Assert.False(comparer.Equals(node2, node3));
        }
    }
}
