﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Logic;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class When_counting_Url_nodes
    {
        [Fact(Timeout=1000)]
        public void Should_be_1_if_single_node()
        { 
            // Arange
            var node = UrlNode.CreateRoot();

            // Act + Assert
            Assert.Equal(1, node.Count);
        }

        [Fact(Timeout=1000)]
        public void Should_be_equal_to_number_of_children_plus_1_in_a_2_level_tree()
        {
            // Arange
            var node = UrlNode.CreateRoot();
            for (int i = 0; i < 4; i++)
            {
                node.AppendChild(i.ToString());
            }

            // Act + Assert
            Assert.Equal(5, node.Count);
        }

        [Fact(Timeout=1000)]
        public void Should_be_able_to_count_nodes_in_a_3_level_tree()
        {
            // Arange
            var node = UrlNode.CreateRoot();
            for (int i = 0; i < 4; i++)
            {
                var child = node.AppendChild(i.ToString());
                for (int j = 0; j < i + 3; j++)
                {
                    child.AppendChild(i.ToString() + "/" + j.ToString());
                }
            }

            // Act + Assert
            Assert.Equal(23, node.Count);
        }
    }
}
