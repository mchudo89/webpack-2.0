﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Logic;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class When_getting_lines_from_UrlNode
    {
        [Fact(Timeout=1000)]
        public void Should_get_1_from_single_node()
        {
            // Arange
            var node = UrlNode.CreateRoot();

            // Act
            var lines = node.Lines();

            // Assert
            Assert.Single(lines);
            Assert.Single(lines.Single());
        }

        [Fact(Timeout=1000)]
        public void Should_get_as_many_as_children()
        {
            // Arange
            var node = UrlNode.CreateRoot();
            for (int i = 0; i < 4; i++)
            {
                node.AppendChild(i.ToString());
            }

            // Act
            var lines = node.Lines();

            // Assert
            Assert.Equal(4, lines.Count());
            foreach (var item in lines)
            {
                var line = item.ToArray();
                Assert.Equal(2, line.Length);
                Assert.ReferenceEquals(line.First(), node);
            }
        }

        [Fact(Timeout=1000)]
        public void Should_be_able_to_get_lines_from_a_3_level_tree()
        {
            // Arange
            var node = UrlNode.CreateRoot();
            var children = new List<UrlNode>();
            for (int i = 0; i < 4; i++)
            {
                var child = node.AppendChild(i.ToString());
                children.Add(child);
                if (i < 3)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        child.AppendChild(i.ToString() + "/" + j.ToString());
                    }
                }
            }

            // Act
            var lines = node.Lines().ToList();
            
            // Assert
            Assert.Equal(7, lines.Count());
            for (int i = 0; i < 7; i++)
            {
                Assert.Equal(i < 6 ? 3 : 2, lines[i].Count());
                Assert.ReferenceEquals(lines[i].First(), node);
                Assert.ReferenceEquals(lines[i].Skip(1).First(), children[i / 4]);
            }
        }
    }
}
