﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Logic;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class When_iterating_through_descendants
    {
        [Fact(Timeout=1000)]
        public void Single_node_should_be_1()
        {
            // Arange
            var node = UrlNode.CreateRoot();

            // Act + Assert
            Assert.Single(node.DescendantsAndSelf());
            Assert.ReferenceEquals(node, node.DescendantsAndSelf().Single());
            
            Assert.Empty(node.Descendants());
        }

        [Fact(Timeout=1000)]
        public void Should_be_equal_to_number_of_children()
        {
            // Arange
            var node = UrlNode.CreateRoot();
            for (int i = 0; i < 4; i++)
            {
                node.AppendChild(i.ToString());
            }

            // Act + Assert
            Assert.Equal(5, new HashSet<UrlNode>(node.DescendantsAndSelf(), new UrlNodeEqualityComparer()).Count);
            Assert.Equal(4, new HashSet<UrlNode>(node.Descendants(), new UrlNodeEqualityComparer()).Count);
        }

        [Fact(Timeout=1000)]
        public void Should_be_able_to_iterate_nodes_in_a_3_level_tree()
        {
            // Arange
            var node = UrlNode.CreateRoot();
            for (int i = 0; i < 4; i++)
            {
                var child = node.AppendChild(i.ToString());
                for (int j = 0; j < i + 3; j++)
                {
                    child.AppendChild(i.ToString() + "/" + j.ToString());
                }
            }

            // Act + Assert
            Assert.Equal(23, new HashSet<UrlNode>(node.DescendantsAndSelf(), new UrlNodeEqualityComparer()).Count());
            Assert.Equal(22, new HashSet<UrlNode>(node.Descendants(), new UrlNodeEqualityComparer()).Count());
        }

    }
}
