﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Logic;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis.MenuAnalysis
{
    public class When_merging_line_to_url_node
    {
        [Fact(Timeout=1000)]
        public void Should_change_tree_if_line_already_in_tree()
        {
            // Arange
            var factory = UrlNode.CreateRoot();
            var line1 = new[] { factory.AppendChild("1") };

            var node1 = UrlNode.CreateRoot();
            node1.AppendChild("1");
            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1").AppendChild("2");

            // Act
            node1.MergeLine(line1);
            node2.MergeLine(line1);

            // Assert
            Assert.Single(node1.Children);
            Assert.Equal(0, node1.Children.Single().Children.Count);
            Assert.True(node1.Children[0].Path == "1");
            Assert.Single(node2.Children);
            Assert.Single(node2.Children.Single().Children);
            Assert.True(node2.Children[0].Path == "1" && node2.Children[0].Children[0].Path == "2");
        }

        [Fact(Timeout=1000)]
        public void Should_insert_line_under_root_if_line_not_in_tree()
        {
            // Arange
            var factory = UrlNode.CreateRoot();
            var line1 = new[] { factory.AppendChild("x") };

            var node1 = UrlNode.CreateRoot();
            node1.AppendChild("1");
            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1").AppendChild("2");

            // Act
            node1.MergeLine(line1);
            node2.MergeLine(line1);

            // Assert
            Assert.Equal(2, node1.Children.Count);
            Assert.Contains("1", node1.Children.Select(n => n.Path));
            Assert.Contains("x", node1.Children.Select(n => n.Path));
            Assert.Equal(2, node2.Children.Count);
            Assert.Contains("1", node2.Children.Select(n => n.Path));
            Assert.Contains("x", node2.Children.Select(n => n.Path));
        }

        [Fact(Timeout=1000)]
        public void Should_insert_line_under_common_node_if_line_in_tree()
        {
            // Arange
            var factory = UrlNode.CreateRoot();
            var line1 = new[] { factory.AppendChild("1"), factory.AppendChild("x") };

            var node1 = UrlNode.CreateRoot();
            node1.AppendChild("1");
            var node2 = UrlNode.CreateRoot();
            node2.AppendChild("1").AppendChild("2");

            // Act
            node1.MergeLine(line1);
            node2.MergeLine(line1);

            // Assert
            Assert.Single(node1.Children);
            Assert.Equal("1", node1.Children.Single().Path);
            Assert.Single(node1.Children.Single().Children);
            Assert.Equal("x", node1.Children.Single().Children.Single().Path);

            Assert.Single(node2.Children);
            Assert.Equal("1", node2.Children.Single().Path);
            Assert.Equal(2, node2.Children.Single().Children.Count);
            Assert.Contains("x", node2.Children.Single().Children.Select(n => n.Path));
        }

    }
}
