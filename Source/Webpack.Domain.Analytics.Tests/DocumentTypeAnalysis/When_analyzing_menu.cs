﻿using FakeItEasy;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using Webpack.Domain.Analytics.DocumentTypeAnalysis.HtmlMapping;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis
{
    public class When_analyzing_menu
    {
        [Fact(Timeout=1000)]
        public void Should_throw_if_null_arguments_in_constructor()
        {
            // Arange + Act + Assert
            Assert.Throws<ArgumentNullException>(() => new MenuAnalyzer(null, null, null));
            Assert.Throws<ArgumentNullException>(() => new MenuAnalyzer(null, null));

            var fakePropertyFactory = A.Fake<IPropertyFactory>();
            Assert.Throws<ArgumentNullException>(() => new MenuAnalyzer(fakePropertyFactory, (string[])null));
            var result = new MenuAnalyzer(fakePropertyFactory);
            Assert.NotNull(result);
        }

        [Fact(Timeout=1000)]
        public void Should_insert_a_property_where_defined()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var propertyDTO = propertyFactory.Get(1);
            var fakePropertyNameState = A.Fake<IPropertyFactory>();
            A.CallTo(() => fakePropertyNameState.GetNew(A<string>.Ignored)).Returns(propertyDTO);
            
            var textSkeleton = "<html><head><title>Title</title></head><body><div>Menu</div></body></html>";
            var skeleton = HtmlNode.CreateNode(textSkeleton);

            var menuAnalyzer = new MenuAnalyzer(fakePropertyNameState, "/html/body/div");

            // Act
            menuAnalyzer.IdentifyMenus(skeleton);

            // Assert
            Assert.Equal(string.Format("<html><head><title>Title</title></head><body>{0}</body></html>", propertyDTO.TemplateReference), skeleton.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_new_property_on_each_occurence()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var propertyDTO1 = propertyFactory.Get(1);
            var propertyDTO2 = propertyFactory.Get(2);

            var fakePropertyNameState = A.Fake<IPropertyFactory>();
            A.CallTo(() => fakePropertyNameState.GetNew(A<string>.Ignored)).ReturnsNextFromSequence(propertyDTO1, propertyDTO2);

            var textSkeleton = "<html><head><title>Title</title></head><body><div>Menu</div><div>Not Menu</div></body></html>";
            var skeleton = HtmlNode.CreateNode(textSkeleton);

            var menuAnalyzer = new MenuAnalyzer(fakePropertyNameState, "/html/body/div");

            // Act
            menuAnalyzer.IdentifyMenus(skeleton);

            // Assert
            Assert.Equal(string.Format("<html><head><title>Title</title></head><body>{0}{1}</body></html>", propertyDTO1.TemplateReference, propertyDTO2.TemplateReference), skeleton.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_insert_a_property_when_menu_defined_by_attribute()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var propertyDTO = propertyFactory.Get(1);
            var fakePropertyNameState = A.Fake<IPropertyFactory>();
            A.CallTo(() => fakePropertyNameState.GetNew(A<string>.Ignored)).Returns(propertyDTO);
            
            var textSkeleton = "<html><head><title>Title</title></head><body><div id=\"menu\">Menu</div><div>Not Menu</div></body></html>";
            var skeleton = HtmlNode.CreateNode(textSkeleton);

            var menuAnalyzer = new MenuAnalyzer(fakePropertyNameState, "//*[@id='menu']");

            // Act
            menuAnalyzer.IdentifyMenus(skeleton);

            // Assert
            Assert.Equal(string.Format("<html><head><title>Title</title></head><body>{0}<div>Not Menu</div></body></html>", propertyDTO.TemplateReference), skeleton.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_insert_a_property_when_menu_is_deep_within_page()
        {
            // Arange
            var propertyFactory = new PropertyFactory();
            var propertyDTO = propertyFactory.Get(1);
            var fakePropertyNameState = A.Fake<IPropertyFactory>();
            A.CallTo(() => fakePropertyNameState.GetNew(A<string>.Ignored)).Returns(propertyDTO);

            var textSkeleton = "<html><head><title>Title</title></head><body><div><div><div><div id=\"menu\">Menu</div><div>Not Menu</div></div></div></div></body></html>";
            var skeleton = HtmlNode.CreateNode(textSkeleton);

            var menuAnalyzer = new MenuAnalyzer(fakePropertyNameState, "//*[@id='menu']");

            // Act
            menuAnalyzer.IdentifyMenus(skeleton);

            // Assert
            Assert.Equal(string.Format("<html><head><title>Title</title></head><body><div><div><div>{0}<div>Not Menu</div></div></div></div></body></html>", propertyDTO.TemplateReference), skeleton.WriteTo());
        }
    }
}
