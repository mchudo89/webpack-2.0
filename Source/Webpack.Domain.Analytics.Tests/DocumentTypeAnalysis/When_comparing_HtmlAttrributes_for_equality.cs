﻿// <copyright file="When_comparing_HtmlAttrributes_for_equality.cs" company="ÚVT MU">
//     Copyright (c) ÚVT MU. All rights reserved.
// </copyright>
// <author>Matej Chudo</author>
namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis
{
    using System.Collections.Generic;
    using HtmlAgilityPack;
    using Webpack.Domain.Analytics.DocumentTypeAnalysis;
    using Xunit;

    /// <summary>
    /// Unit tests of <seealso cref="HtmlAttributeEqualityComparer" />.
    /// </summary>
    public class When_comparing_HtmlAttrributes_for_equality
    {
        /// <summary>
        /// Tests for null arguments.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_if_both_are_null()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();
            HtmlAttribute att1 = null;
            HtmlAttribute att2 = null;

            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for 1 null argument.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_false_if_one_is_null()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();
            
            HtmlAttribute att1 = HtmlNode.CreateNode(@"<p class=""class""></p>").Attributes[0];
            HtmlAttribute att2 = null;

            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.False(result);
        }

        /// <summary>
        /// Tests for equality attributes with same name and value, but value with different case.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_for_attributes_with_same_name_and_value_with_different_case()
        {
            // Arange
            HtmlAttribute att1 = HtmlNode.CreateNode(@"<p name=""Value""></p>").Attributes[0];
            HtmlAttribute att2 = HtmlNode.CreateNode(@"<p name=""VALUE""></p>").Attributes[0];

            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();
            
            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for equality attributes with same name and value, but name and value with different case.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_for_attributes_with_name_and_value_with_different_case()
        {
            // Arange
            HtmlAttribute att1 = HtmlNode.CreateNode(@"<p name=""Value""></p>").Attributes[0];
            HtmlAttribute att2 = HtmlNode.CreateNode(@"<p Name=""VALUE""></p>").Attributes[0];

            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();

            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for equality attributes with same name and value with same case.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_for_attributes_with_same_name_and_value_with_same_case()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();

            HtmlAttribute att1 = HtmlNode.CreateNode(@"<p class=""VALUE""></p>").Attributes[0];
            HtmlAttribute att2 = HtmlNode.CreateNode(@"<p class=""VALUE""></p>").Attributes[0];

            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for equality attributes with same name and different value.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_false_for_attributes_with_same_name_and_different_value()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();

            HtmlAttribute att1 = HtmlNode.CreateNode(@"<p class=""VALUE1""></p>").Attributes[0];
            HtmlAttribute att2 = HtmlNode.CreateNode(@"<p class=""VALUE2""></p>").Attributes[0];
            
            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.False(result);
        }

        /// <summary>
        /// Tests for equality for different attributes.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_false_for_attributes_with_same_value_and_different_name()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();

            HtmlAttribute att1 = HtmlNode.CreateNode(@"<p class=""value""></p>").Attributes[0];
            HtmlAttribute att2 = HtmlNode.CreateNode(@"<p id=""value""></p>").Attributes[0];

            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.False(result);
        }

        /// <summary>
        /// Tests for equality for different attributes.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_false_for_attributes_with_different_name_and_value()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> comparer = new HtmlAttributeEqualityComparer();

            HtmlAttribute att1 = HtmlNode.CreateNode(@"<p class=""value1""></p>").Attributes[0];
            HtmlAttribute att2 = HtmlNode.CreateNode(@"<p id=""value2""></p>").Attributes[0];
            
            // Act
            bool result = comparer.Equals(att1, att2);

            // Assert
            Assert.False(result);
        }
    }
}
