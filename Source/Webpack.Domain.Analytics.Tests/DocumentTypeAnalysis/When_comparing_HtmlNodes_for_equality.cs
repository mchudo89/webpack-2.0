﻿// <copyright file="When_comparing_HtmlNodes_for_equality.cs" company="ÚVT MU">
//     Copyright (c) ÚVT MU. All rights reserved.
// </copyright>
// <author>Matej Chudo</author>
namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis
{
    using System;
    using System.Collections.Generic;
    using FakeItEasy;
    using HtmlAgilityPack;
    using Webpack.Domain.Analytics.DocumentTypeAnalysis;
    using Xunit;

    /// <summary>
    /// Unit test for HtmlNodeEqualityComparer
    /// </summary>
    public class When_comparing_HtmlNodes_for_equality
    {
        /// <summary>
        /// Tests for null value.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_if_both_are_null()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> attComparer = A.Fake<IEqualityComparer<HtmlAttribute>>();
            IEqualityComparer<HtmlNode> comparer = new HtmlNodeEqualityComparer(attComparer);
            HtmlNode node1 = null;
            HtmlNode node2 = null;

            // Act
            bool result = comparer.Equals(node1, node2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for 1 null value.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_false_if_one_is_null()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> attComparer = A.Fake<IEqualityComparer<HtmlAttribute>>();
            IEqualityComparer<HtmlNode> comparer = new HtmlNodeEqualityComparer(attComparer);
            HtmlNode node1 = HtmlNode.CreateNode("<p></p>");
            HtmlNode node2 = null;

            // Act
            bool result = comparer.Equals(node1, node2);

            // Assert
            Assert.False(result);
        }

        /// <summary>
        /// Tests for same nodes.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_for_nodes_with_same_name_type_and_attributes()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> attComparer = A.Fake<IEqualityComparer<HtmlAttribute>>();
            IEqualityComparer<HtmlNode> comparer = new HtmlNodeEqualityComparer(attComparer);
            HtmlNode node1 = HtmlNode.CreateNode(@"<p class=""class""></p>");
            HtmlNode node2 = HtmlNode.CreateNode(@"<p class=""class""></p>");
            
            A.CallTo(() => attComparer.Equals(A<HtmlAttribute>.Ignored, A<HtmlAttribute>.Ignored)).Returns(true);

            // Act
            bool result = comparer.Equals(node1, node2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for same nodes but with attributes in different order.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_for_nodes_with_same_name_type_and_attributes_in_different_order()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> attComparer = A.Fake<IEqualityComparer<HtmlAttribute>>();
            IEqualityComparer<HtmlNode> comparer = new HtmlNodeEqualityComparer(attComparer);
            HtmlNode node1 = HtmlNode.CreateNode(@"<p class=""class"" id=""sometihng""></p>");
            HtmlNode node2 = HtmlNode.CreateNode(@"<p id=""sometihng"" class=""class""></p>");
            
            A.CallTo(() => attComparer.Equals(A<HtmlAttribute>.Ignored, A<HtmlAttribute>.Ignored))
                .ReturnsLazily<bool, HtmlAttribute, HtmlAttribute>((a1, a2) => string.Equals(a1.Name, a2.Name, StringComparison.InvariantCultureIgnoreCase)
                    && string.Equals(a1.Value, a2.Value, StringComparison.InvariantCultureIgnoreCase));
            
            // Act
            bool result = comparer.Equals(node1, node2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for nodes with different casing.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_true_for_nodes_with_same_name_type_and_attributes_case_insensitive()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> attComparer = A.Fake<IEqualityComparer<HtmlAttribute>>();
            IEqualityComparer<HtmlNode> comparer = new HtmlNodeEqualityComparer(attComparer);
            
            HtmlNode node1 = HtmlNode.CreateNode(@"<p class=""class"" id=""something""></p>");
            HtmlNode node2 = HtmlNode.CreateNode(@"<P id=""SOMETHING"" class=""class""></P>");

            A.CallTo(() => attComparer.Equals(A<HtmlAttribute>.Ignored, A<HtmlAttribute>.Ignored))
                .ReturnsLazily<bool, HtmlAttribute, HtmlAttribute>((a1, a2) => string.Equals(a1.Name, a2.Name, StringComparison.InvariantCultureIgnoreCase)
                    && string.Equals(a1.Value, a2.Value, StringComparison.InvariantCultureIgnoreCase));

            // Act
            bool result = comparer.Equals(node1, node2);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Tests for nodes but with different attributes.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_false_for_nodes_with_same_name_type_and_diferent_attributes()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> attComparer = A.Fake<IEqualityComparer<HtmlAttribute>>();
            IEqualityComparer<HtmlNode> comparer = new HtmlNodeEqualityComparer(attComparer);
            HtmlNode node1 = HtmlNode.CreateNode(@"<p id=""sometihng""></p>");
            HtmlNode node2 = HtmlNode.CreateNode(@"<p class=""class""></p>");
            A.CallTo(() => attComparer.Equals(A<HtmlAttribute>.Ignored, A<HtmlAttribute>.Ignored)).Returns(false);

            // Act
            bool result = comparer.Equals(node1, node2);

            // Assert
            Assert.False(result);
        }

        /// <summary>
        /// Tests for text item and element.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_be_false_for_nodes_with_diferent_type()
        {
            // Arange
            IEqualityComparer<HtmlAttribute> attComparer = A.Fake<IEqualityComparer<HtmlAttribute>>();
            IEqualityComparer<HtmlNode> comparer = new HtmlNodeEqualityComparer(attComparer);
            HtmlNode node1 = HtmlNode.CreateNode(@"<p></p>");
            HtmlNode node2 = HtmlNode.CreateNode(@"p");

            // Act
            bool result = comparer.Equals(node1, node2);

            // Assert
            Assert.False(result);
        }
    }
}
