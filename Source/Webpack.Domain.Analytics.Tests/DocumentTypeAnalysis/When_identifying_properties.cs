﻿namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis
{
    using FakeItEasy;
    using HtmlAgilityPack;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Webpack.Domain.Analytics.DocumentTypeAnalysis;
    using Webpack.Domain.Analytics.DocumentTypeAnalysis.HtmlMapping;
    using Webpack.Domain.Model.Entities;
    using Webpack.Domain.Model.Logic;
    using Xunit;
    using Xunit.Extensions;

    public class When_identifying_properties
    {
        [Fact(Timeout=1000)]
        public void Should_find_no_properties_in_page_identical_to_skeleton()
        {
            // Arange
            var page = @"<html><head><title>Test Page</title></head><body></body></html>";
            var skeletonDoc = new HtmlDocument();
            skeletonDoc.LoadHtml(page);

            // Act
            var propertyIdentifier = new PropertyIdentifier(skeletonDoc.DocumentNode, GetBuilder());
            propertyIdentifier.Visit(new Page { RawPage = new RawPage("/test".GetTestUri(), 
                skeletonDoc.DocumentNode.WriteTo(), string.Empty, Enumerable.Empty<Uri>()) });

            // Assert
            var result = propertyIdentifier.PopulatedSkeleton;
            Assert.NotNull(result);
            Assert.Equal(page, result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_property_in_page()
        {
            // Arange
            var skeleton = @"<html><head><title>Test Page</title></head><body></body></html>";
            var page = @"<html><head><title>Test Page</title></head><body>BODY</body></html>";
            var skeletonDoc = new HtmlDocument();
            skeletonDoc.LoadHtml(skeleton);

            // Act
            var propertyIdentifier = new PropertyIdentifier(skeletonDoc.DocumentNode, GetBuilder());
            propertyIdentifier.Visit(new Page { RawPage = new RawPage("/test".GetTestUri(), page, 
                string.Empty, Enumerable.Empty<Uri>()) });

            // Assert
            var result = propertyIdentifier.PopulatedSkeleton;
            Assert.NotNull(result);
            Assert.Equal(@"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>", result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_properties_in_page()
        {
            // Arange
            var skeleton = @"<html><head><title>Test Page</title></head><body></body></html>";
            var page = @"<html><head><title>Test Page</title></head><body>BODY<div>Text</div></body></html>";
            var skeletonDoc = new HtmlDocument();
            skeletonDoc.LoadHtml(skeleton);

            // Act
            var propertyIdentifier = new PropertyIdentifier(skeletonDoc.DocumentNode, GetBuilder());
            propertyIdentifier.Visit(new Page { RawPage = new RawPage("/test".GetTestUri(), page, 
                string.Empty, Enumerable.Empty<Uri>()) });

            // Assert
            var result = propertyIdentifier.PopulatedSkeleton;
            Assert.NotNull(result);
            Assert.Equal(@"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>", result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_group_properties_in_page_under_existing_properties()
        {
            // Arange
            var skeleton = @"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>";
            var page = @"<html><head><title>Test Page</title></head><body>BODY</body></html>";
            var skeletonDoc = new HtmlDocument();
            skeletonDoc.LoadHtml(skeleton);

            // Act
            var propertyIdentifier = new PropertyIdentifier(skeletonDoc.DocumentNode, GetBuilder());
            propertyIdentifier.Visit(new Page { RawPage = new RawPage("/test".GetTestUri(), page, 
                string.Empty, Enumerable.Empty<Uri>()) });

            // Assert
            var result = propertyIdentifier.PopulatedSkeleton;
            Assert.NotNull(result);
            Assert.Equal(@"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>", result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_properties_in_3_pages()
        {
            // Arange
            var skeleton = @"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>";
            var page1 = @"<html><head><title>Test Page</title></head><body>BODY1</body></html>";
            var page2 = @"<html><head><title>Test Page</title></head><body>BODY2</body></html>";
            var skeletonDoc = new HtmlDocument();
            skeletonDoc.LoadHtml(skeleton);

            // Act
            var propertyIdentifier = new PropertyIdentifier(skeletonDoc.DocumentNode, GetBuilder());
            propertyIdentifier.Visit(new Page { RawPage = new RawPage("/test".GetTestUri(), page1, string.Empty, Enumerable.Empty<Uri>()) });
            propertyIdentifier.Visit(new Page { RawPage = new RawPage("/test".GetTestUri(), page2, string.Empty, Enumerable.Empty<Uri>()) });

            // Assert
            var result = propertyIdentifier.PopulatedSkeleton;
            Assert.NotNull(result);
            Assert.Equal(@"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>", result.WriteTo());
        }

        [Theory, ClassData(typeof(WebsiteContentProvider))]
        public void Shoud_find_properties_of_a_group_of_pages(Tuple<string, string>[] data)
        {
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            var pages = data.Select(d => new Page
            {
                RawPage = new RawPage(d.Item1.GetTestUri(), d.Item2, string.Empty, Enumerable.Empty<Uri>())
            }).ToList();

            pages.ForEach(htmlComparer.Visit);

            var propertyIdentifier = new PropertyIdentifier(htmlComparer.Result, GetBuilder());
            pages.ForEach(propertyIdentifier.Visit);

            var result = propertyIdentifier.PopulatedSkeleton.WriteTo();
            Assert.NotNull(result);
        }

        //[Theory, ClassData(typeof(WebsiteContentProvider))]
        //public void Should_find_DocumentTypes(Tuple<string, string>[] data)
        //{
        //    // Arange
        //    var menuAnalyzerFake = A.Fake<IMenuAnalyzer>();
        //    A.CallTo(() => menuAnalyzerFake.IdentifyMacros(A<HtmlNode>.Ignored)).ReturnsLazily(args => (HtmlNode)args.Arguments.First());
        //    var documentTypeAnalyzer = new DocumentTypeAnalyzer(new PageComparingFactory(), new DocumentTypeFactory(), menuAnalyzerFake);
        //    var rawPages = data.Select(d => new Page
        //    {
        //        RawPage = new RawPage(d.Item1, d.Item2, Enumerable.Empty<Uri>())
        //    }).ToList();

        //    // Act
        //    var result = documentTypeAnalyzer.DetermineDocumentTypes(rawPages);

        //    // Assert
        //    Assert.NotNull(result);
        //}

        [Fact(Timeout=1000)]
        public void Should_not_discard_property_if_is_empty_in_page()
        {
            // Arange
            var skeleton = @"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>";
            var pageText = @"<html><head><title>Test Page</title></head><body></body></html>";
            var skeletonDoc = new HtmlDocument();
            skeletonDoc.LoadHtml(skeleton);
            var page = new Page { RawPage = new RawPage("/test".GetTestUri(), pageText, string.Empty, Enumerable.Empty<Uri>()) };

            // Act
            var propertyIdentifier = new PropertyIdentifier(skeletonDoc.DocumentNode, GetBuilder());
            propertyIdentifier.Visit(page);

            // Assert
            var result = propertyIdentifier.PopulatedSkeleton;
            Assert.NotNull(result);
            Assert.Equal(@"<html><head><title>Test Page</title></head><body>@Raw(Model.Property0)</body></html>", result.WriteTo());
            Assert.NotEmpty(page.Properties);
        }

        private static Builder GetBuilder()
        {
            return new Builder(new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer()), new PropertyFactory());
        }
    }
}
