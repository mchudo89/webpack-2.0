﻿// <copyright file="When_searching_for_page_skeleton.cs" company="ÚVT MU">
//     Copyright (c) ÚVT MU. All rights reserved.
// </copyright>
// <author>Matej Chudo</author>
namespace Webpack.Domain.Analytics.Tests.DocumentTypeAnalysis
{
    using System;
    using System.IO;
    using System.Linq;
    using Webpack.Domain.Analytics.DocumentTypeAnalysis;
    using Webpack.Domain.Model.Entities;
    using Xunit;
    using Xunit.Extensions;

    /// <summary>
    /// Tests for searching for page skeleton.
    /// </summary>
    public class When_searching_for_page_skeleton
    {
        /// <summary>
        /// Tests constructor for null argument.
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_throw_when_null_arguments_in_constructor()
        {
            // Arange + Act + Assert
            Assert.Throws<ArgumentNullException>(() => new SkeletonExtractor(null));
        }

        [Fact(Timeout=1000)]
        public void Should_find_same_skeleton_as_input()
        {
            // Arange
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            // Act
            string html = "<html><head><title>Nejaky nadpis</title></head><body></body></html>";
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(1.GetTestUri(), html, string.Empty, Enumerable.Empty<Uri>())
            });
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(2.GetTestUri(), html, string.Empty, Enumerable.Empty<Uri>())
            });

            var result = htmlComparer.Result;

            // Assert
            Assert.Equal(html, result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_skeleton_equal_to_subset_input_1()
        {
            // Arange
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            // Act
            string html1 = "<html><head><title>Nejaky nadpis</title></head><body></body><div></div></html>";
            string html2 = "<html><head><title>Nejaky nadpis</title></head><body></body></html>";
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(1.GetTestUri(), html1, string.Empty, Enumerable.Empty<Uri>())
            });
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(2.GetTestUri(), html2, string.Empty, Enumerable.Empty<Uri>())
            });

            var result = htmlComparer.Result;

            // Assert
            Assert.Equal(html2, result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_skeleton_equal_to_subset_input_2()
        {
            // Arange
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            // Act
            string html1 = "<html><head><title>Nejaky nadpis</title></head><body></body></html>";
            string html2 = "<html><head><title>Nejaky nadpis</title></head><body></body><div></div></html>";
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(1.GetTestUri(), html1, string.Empty, Enumerable.Empty<Uri>())
            });
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(2.GetTestUri(), html2, string.Empty, Enumerable.Empty<Uri>())
            });

            var result = htmlComparer.Result;

            // Assert
            Assert.Equal(html1, result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_skeleton_equal_to_intersection_of_2()
        {
            // Arange
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            // Act
            string html1 = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""x2""></div></body></html>";
            string html2 = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""x1""></div></body></html>";
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(1.GetTestUri(), html1, string.Empty, Enumerable.Empty<Uri>())
            });
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(2.GetTestUri(), html2, string.Empty, Enumerable.Empty<Uri>())
            });

            var result = htmlComparer.Result;

            // Assert
            var expected = @"<html><head><title>Nejaky nadpis</title></head><body></body></html>";
            Assert.Equal(expected, result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_skeleton_equal_to_intersection_of_3()
        {
            // Arange
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            // Act
            string html1 = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""x1""></div></body></html>";
            string html2 = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""x2""></div></body></html>";
            string html3 = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""x3""></div></body></html>";
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(1.GetTestUri(), html1, string.Empty, Enumerable.Empty<Uri>())
            });
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(2.GetTestUri(), html2, string.Empty, Enumerable.Empty<Uri>())
            });
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(3.GetTestUri(), html3, string.Empty, Enumerable.Empty<Uri>())
            });

            var result = htmlComparer.Result;

            // Assert
            var expected = @"<html><head><title>Nejaky nadpis</title></head><body></body></html>";
            Assert.Equal(expected, result.WriteTo());
        }

        [Fact(Timeout=1000)]
        public void Should_find_skeleton_of_2_pages()
        {
            // Arange
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            // Act
            string html1 = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""menu"">Menu</div><div id=""content""><div id=""1"">x</div><div id=""2""></div></div></body></html>";
            string html2 = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""menu"">Menu</div><div id=""content""><div id=""2""></div><div id=""1"">y</div></div></body></html>";
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(1.GetTestUri(), html1, string.Empty, Enumerable.Empty<Uri>())
            });
            htmlComparer.Visit(new Page
            {
                RawPage = new RawPage(2.GetTestUri(), html2, string.Empty, Enumerable.Empty<Uri>())
            });

            var result = htmlComparer.Result;

            // Assert
            var expected = @"<html><head><title>Nejaky nadpis</title></head><body><div id=""menu"">Menu</div><div id=""content""><div id=""1""></div></div></body></html>";
            Assert.Equal(expected, result.WriteTo());
        }

        [Theory, ClassData(typeof(WebsiteContentProvider))]
        public void Shoud_find_skeleton_of_a_group_of_pages(Tuple<string, string>[] data)
        {
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var htmlComparer = new SkeletonExtractor(comparer);

            foreach (var html in data)
            {
                htmlComparer.Visit(new Page
                {
                    RawPage = new RawPage(html.Item1.GetTestUri(), html.Item2, string.Empty, Enumerable.Empty<Uri>())
                });
            }

            var result = htmlComparer.Result;
            Assert.NotNull(result);
            Assert.NotNull(result.ChildNodes["html"]);
        }
    }
}
