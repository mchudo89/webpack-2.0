﻿// <copyright file="When_analyzing_hierarchy.cs" company="ÚVT MU">
//     Copyright (c) ÚVT MU. All rights reserved.
// </copyright>
// <author>Matej Chudo</author>
namespace Webpack.Domain.Analytics.Tests.HierarchyAnalysis
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Webpack.Domain.Analytics.HierarchyAnalysis;
    using Webpack.Domain.Model.Entities;
    using Xunit;

    /// <summary>
    /// Unit tests of <seealso cref="HierarchyAnalyzer" />, method <seealso cref="HierarchyAnalyzer.DetermineHierarchy"/>
    /// </summary>
    public class When_analyzing_hierarchy
    {
        /// <summary>
        /// Test if ArgumentNullException is thrown when trying to determine the hierarchy of null
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_throw_if_rawPages_is_null()
        {
            // Arange
            var analyzer = new HierarchyAnalyzer();

            // Act + Assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                analyzer.DetermineHierarchy(null);
            });
        }

        /// <summary>
        /// Test if a childless rootUrlNode is returned for an empty list
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_return_childless_root_when_rawPages_is_empty()
        {
            // Arange
            var analyzer = new HierarchyAnalyzer();

            // Act
            var result = analyzer.DetermineHierarchy(new List<Page>());
            
            // Assert
            Assert.NotNull(result);

            var root = result.Root;
            Assert.NotNull(root);
            Assert.Null(root.Parent);
            Assert.Empty(root.Children);
            Assert.Equal(root.Name, string.Empty);
        }

        /// <summary>
        /// Test if is able to recognize directories, create hierarchy and assign raw rawPages
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_recognize_simple_directory_structure()
        {
            // Arrange
            var analyzer = new HierarchyAnalyzer();
            var pages = new List<Page>
            {
                new Page { RawPage = new RawPage("/dir1/page1".GetTestUri(), "data1", string.Empty, Enumerable.Empty<Uri>()) },
                new Page { RawPage = new RawPage("/dir1/page2".GetTestUri(), "data2", string.Empty, Enumerable.Empty<Uri>()) },
                new Page { RawPage = new RawPage("/".GetTestUri(), "data3", string.Empty, Enumerable.Empty<Uri>()) }
            };

            // Act
            var result = analyzer.DetermineHierarchy(pages);

            // Assert
            Assert.NotNull(result);

            // rootUrlNode
            var root = result.Root;
            Assert.NotNull(root);
            Assert.Equal(string.Empty, root.Name);
            Assert.NotNull(root.RawPage);
            Assert.Null(root.Parent);
            Assert.Equal(1, root.Children.Count);

            var directory = root.Children.Single();
            Assert.Equal("dir1", directory.Name);
            Assert.Null(directory.RawPage);
            Assert.Equal(2, directory.Children.Count);
            Assert.Same(root, directory.Parent);

            var page1 = directory.Children[0];
            Assert.Equal("page1", page1.Name);
            Assert.NotNull(page1.RawPage);
            Assert.Same(directory, page1.Parent);
            Assert.Empty(page1.Children);

            var page2 = directory.Children[1];
            Assert.Equal("page2", page2.Name);
            Assert.NotNull(page2.RawPage);
            Assert.Same(directory, page2.Parent);
            Assert.Empty(page2.Children);
        }

        /// <summary>
        /// Test if is able to recognize directories from a more complicated structure, create hierarchy and assign raw rawPages
        /// </summary>
        [Fact(Timeout=1000)]
        public void Should_recognize_complicated_directory_structure()
        {
            // Arrange
            var analyzer = new HierarchyAnalyzer();
            var pages = new List<Page>
            {
                new Page { RawPage = new RawPage("/dir1/page1".GetTestUri(), "data1", string.Empty, Enumerable.Empty<Uri>()) },
                new Page { RawPage = new RawPage("/dir1/page2".GetTestUri(), "data2", string.Empty, Enumerable.Empty<Uri>()) },
                new Page { RawPage = new RawPage("/dir2/page1".GetTestUri(), "data3", string.Empty, Enumerable.Empty<Uri>()) },
                new Page { RawPage = new RawPage("/dir2/page2".GetTestUri(), "data4", string.Empty, Enumerable.Empty<Uri>()) },
                new Page { RawPage = new RawPage("/dir2/page3".GetTestUri(), "data5", string.Empty, Enumerable.Empty<Uri>()) },
                new Page { RawPage = new RawPage("/dir2".GetTestUri(), "data6", string.Empty, Enumerable.Empty<Uri>()) },
            };

            // Act
            var result = analyzer.DetermineHierarchy(pages);

            // Assert
            Assert.NotNull(result);

            // rootUrlNode
            var root = result.Root;
            Assert.NotNull(root);
            Assert.Equal(string.Empty, root.Name);
            Assert.Null(root.Parent);
            Assert.Null(root.RawPage);
            Assert.Equal(2, root.Children.Count);

            // directories
            var directory1 = root.Children[0];
            Assert.Equal("dir1", directory1.Name);
            Assert.Null(directory1.RawPage);
            Assert.Equal(2, directory1.Children.Count);
            Assert.Same(root, directory1.Parent);

            var directory2 = root.Children[1];
            Assert.Equal("dir2", directory2.Name);
            Assert.NotNull(directory2.RawPage);
            Assert.Equal(3, directory2.Children.Count);
            Assert.Same(root, directory1.Parent);

            // rawPages in dir1
            var page1 = directory1.Children[0];
            Assert.Equal("page1", page1.Name);
            Assert.NotNull(page1.RawPage);
            Assert.Same(directory1, page1.Parent);
            Assert.Empty(page1.Children);

            var page2 = directory1.Children[1];
            Assert.Equal("page2", page2.Name);
            Assert.NotNull(page2.RawPage);
            Assert.Same(directory1, page2.Parent);
            Assert.Empty(page2.Children);

            // rawPages in dir2
            var page3 = directory2.Children[0];
            Assert.Equal("page1", page3.Name);
            Assert.NotNull(page3.RawPage);
            Assert.Same(directory2, page3.Parent);
            Assert.Empty(page3.Children);

            var page4 = directory2.Children[1];
            Assert.Equal("page2", page4.Name);
            Assert.NotNull(page4.RawPage);
            Assert.Same(directory2, page4.Parent);
            Assert.Empty(page4.Children);

            var page5 = directory2.Children[2];
            Assert.Equal("page3", page5.Name);
            Assert.NotNull(page5.RawPage);
            Assert.Same(directory2, page5.Parent);
            Assert.Empty(page5.Children);
        }
    }
}
