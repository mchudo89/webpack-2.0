﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using Webpack.Domain.Analytics.Extensions;
using Webpack.Domain.Analytics.HierarchyAnalysis;
using Webpack.Domain.Model.Logic;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.MenuAnalysis
{
    public class When_searching_for_pages_hierarchy_via_menu_analysis
    {
        [Fact(Timeout=1000)]
        public void Should_throw_when_arguments_null()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);
            
            var root = UrlNode.CreateRoot();
            var data = string.Empty;

            // Act + Assert
            Assert.Throws<ArgumentNullException>(() => menuHierarchyFinder.ExtractHierarchyFromMenu(null));
            Assert.DoesNotThrow(() => menuHierarchyFinder.ExtractHierarchyFromMenu(data));
        }

        [Fact(Timeout=1000)]
        public void Should_find_single_level_hierarchy()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var root = UrlNode.CreateRoot();
            var data = "<ul><li><a href=\"/1\"></a></li><li><a href=\"/2\"></a></li><li><a href=\"/3\"></a></li><li><a href=\"/4\"></a></li></ul>";

            // Act
            var result = menuHierarchyFinder.ExtractHierarchyFromMenu(data);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Children);
            Assert.Equal(4, result.Children.Count);
            for (int i = 0; i < result.Children.Count; i++)
            {
                Assert.Equal("/" + (i+1).ToString(), result.Children[i].Path);
            }
        }

        [Fact(Timeout=1000)]
        public void Should_find_2_level_hierarchy()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var root = UrlNode.CreateRoot();
            var data =
                @"<ul>
                    <li><a href=""/1""></a>
                        <ul>
                            <li><a href=""/1/1""></a></li>
                            <li><a href=""/1/2""></a></li>
                        </ul>
                    </li>
                    <li><a href=""/2""></a>
                        <ul>
                            <li><a href=""/2/1""></a></li>
                            <li><a href=""/2/2""></a></li>
                        </ul>
                    </li>
                    <li><a href=""/3""></a>
                        <ul>
                            <li><a href=""/3/1""></a></li>
                            <li><a href=""/3/2""></a></li>
                            <li><a href=""/3/3""></a></li>
                        </ul>
                    </li>
                </ul>";

            // Act
            var result = menuHierarchyFinder.ExtractHierarchyFromMenu(data);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Children);
            Assert.Equal(3, result.Children.Count);
            for (int i = 0; i < result.Children.Count; i++)
            {
                var upper = result.Children[i];
                Assert.Equal(string.Format("/{0}", i + 1), upper.Path);
                for (int j = 0; j < upper.Children.Count; j++)
                {
                    var lower = upper.Children[j];
                    Assert.Equal(string.Format("/{0}/{1}", i + 1, j + 1), lower.Path);
                }
            }
        }

        [Fact(Timeout=1000)]
        public void Should_find_3_level_hierarchy()
        {
            // Arange
            var comparer = new NodePositionEqualityComparer();
            var menuHierarchyFinder = new MenuHierarchyFinder(comparer);

            var root = UrlNode.CreateRoot();
            var data =
                @"<ul>
                    <li><a href=""/1""></a>
                        <ul>
                            <li><a href=""/1/1""></a>
                                <ul>
                                    <li><a href=""/1/1/1""></a></li>
                                    <li><a href=""/1/1/2""></a></li>
                                </ul>
                            </li>
                            <li><a href=""/1/2""></a></li>
                        </ul>
                    </li>
                    <li><a href=""/2""></a>
                    </li>
                    <li><a href=""/3""></a>
                        <ul>
                            <li><a href=""/3/1""></a></li>
                            <li><a href=""/3/2""></a></li>
                            <li><a href=""/3/3""></a></li>
                        </ul>
                    </li>
                </ul>";

            // Act
            var result = menuHierarchyFinder.ExtractHierarchyFromMenu(data);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Children);
            Assert.Equal(3, result.Children.Count);
            for (int i = 0; i < result.Children.Count; i++)
            {
                var upper = result.Children[i];
                Assert.Equal(string.Format("/{0}", i + 1), upper.Path);
                for (int j = 0; j < upper.Children.Count; j++)
                {
                    var lower = upper.Children[j];
                    Assert.Equal(string.Format("/{0}/{1}", i + 1, j + 1), lower.Path);
                    for (int k = 0; k < lower.Children.Count; k++)
                    {
                        var lowest = lower.Children[k];
                        Assert.Equal(string.Format("/{0}/{1}/{2}", i + 1, j + 1, k + 1), lowest.Path);
                    }
                }
            }
        }
    }
}
