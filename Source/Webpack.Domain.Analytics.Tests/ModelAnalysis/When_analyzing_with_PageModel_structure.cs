﻿namespace Webpack.Domain.Analytics.Tests.ModelAnalysis
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Webpack.Domain.Analytics.ModelAnalysis;
    using Webpack.Domain.Analytics.Extensions;
    using Webpack.Domain.Model.Entities;
    using Xunit;
    using FakeItEasy;

    public class When_analyzing_with_PageModel_structure
    {
        private IEnumerable<RawPage> GetTestPages()
        {
            for (int i = 0; i < 20; i++)
            {
                yield return new RawPage(i.GetTestUri(),
                    string.Format("<html><head><title>Title {0}</title></head><body>{0}</body></html>", i), 
                    string.Empty, Enumerable.Empty<Uri>());
            }
        }

        private Condition<RawPage> GetAllwaysTrue()
        {
            return Condition<RawPage>.Allways();
        }

        private Condition<RawPage> GetAllwaysFalse()
        {
            return Condition<RawPage>.Never();
        }

        [Fact(Timeout=1000)]
        public void Should_throw_if_constructor_args_are_null()
        {
            // Arange + Act + Assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                var pageModel = new PageModel(null);
            });
            Assert.DoesNotThrow(() =>
            {
                var pageModel = new PageModel("model-name");
            });
        }

        [Fact(Timeout=1000)]
        public void Should_respect_meet_condition()
        {
            // Arange
            var pageModel = new PageModel("model-name");
            var pages = GetTestPages();

            // Act
            pageModel.Handle(pages);

            // Assert
            Assert.Empty(pageModel.MetPages);
            Assert.Empty(pageModel.UnhandledPages);
            Assert.Equal(GetTestPages().Cat(rp => rp.Path), pages.Where(p => !p.IsHandled).Cat(rp => rp.Path));
        }

        [Fact(Timeout=1000)]
        public void Should_catch_every_page()
        {
            // Arange
            var pageModel = new PageModel("model-name") { Meets = GetAllwaysTrue(), FallsThrough = GetAllwaysFalse() };
            var pages = GetTestPages();

            // Act
            pageModel.Handle(pages);

            // Assert
            Assert.Empty(pageModel.UnhandledPages);
            Assert.Equal(pages.Count(), pageModel.MetPages.Count);
            Assert.Equal(pages.Cat(rp => rp.Path), pageModel.MetPages.Cat(rp => rp.Path)); 
        }

        [Fact(Timeout=1000)]
        public void Should_pass_pages_that_satisfy_fall_through_condition_to_children()
        {
            // Arange
            var pages = GetTestPages();
            var pageModelChild = new PageModel("model-name") { Meets = GetAllwaysTrue(), FallsThrough = GetAllwaysFalse() };
            var pageModel = new PageModel("model-name") { Meets = GetAllwaysFalse(), FallsThrough = GetAllwaysTrue() };
            pageModel.Children.Add(pageModelChild);

            // Act
            pageModel.Handle(pages);

            // Assert
            Assert.Empty(pageModel.UnhandledPages);
            Assert.Empty(pageModel.MetPages);
            Assert.Empty(pageModelChild.UnhandledPages);
            Assert.Equal(pages.Cat(p => p.Path), pageModelChild.MetPages.Cat(p => p.Path));
        }

        [Fact(Timeout=1000)]
        public void Should_pass_pages_that_satisfy_fall_through_condition_to_children_and_children_should_pick_theirs()
        {
            // Arange
            var pages = GetTestPages();
            var pageModelChild = new PageModel("model-name")
            {
                Meets = Condition<RawPage>.Create().Rule(p => int.Parse(p.Url.LastSegment) < 5),
                FallsThrough = GetAllwaysFalse()
            };

            var pageModel = new PageModel("model-name") { Meets = GetAllwaysFalse(), FallsThrough = GetAllwaysTrue() };
            pageModel.Children.Add(pageModelChild);

            // Act
            pageModel.Handle(pages);

            // Assert
            Assert.Empty(pageModel.MetPages);
            Assert.Equal(15, pageModel.UnhandledPages.Count);
            Assert.Empty(pageModelChild.UnhandledPages);
            Assert.Equal(5, pageModelChild.MetPages.Count);
        }

        [Fact(Timeout=1000)]
        public void Should_pass_pages_that_satisfy_fall_through_condition_to_both_children_and_both_children_should_pick_theirs()
        {
            // Arange
            var pages = GetTestPages();
            var pageModelChild1 = new PageModel("model-name") 
            {
                Meets = Condition<RawPage>.Create().Rule(p => int.Parse(p.Url.LastSegment) < 5),
                FallsThrough = GetAllwaysFalse()
            };
            var pageModelChild2 = new PageModel("model-name") 
            {
                Meets = Condition<RawPage>.Create().Rule(p => int.Parse(p.Url.LastSegment) < 10),
                FallsThrough = GetAllwaysFalse()
            };
            var pageModel = new PageModel("model-name")
            {
                Meets = GetAllwaysFalse(),
                FallsThrough = GetAllwaysTrue()
            }; ;
            pageModel.Children.Add(pageModelChild1);
            pageModel.Children.Add(pageModelChild2);

            // Act
            pageModel.Handle(pages);

            // Assert
            Assert.Empty(pageModel.MetPages);
            Assert.Equal(10, pageModel.UnhandledPages.Count);
            Assert.Equal(5, pageModelChild1.MetPages.Count);
            Assert.Empty(pageModelChild1.UnhandledPages);
            Assert.Equal(5, pageModelChild2.MetPages.Count);
            Assert.Empty(pageModelChild2.UnhandledPages);
        }
    }
}
