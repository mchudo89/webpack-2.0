﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.ModelAnalysis;
using Webpack.Domain.Model.Entities;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.ModelAnalysis
{
    public class When_using_ArticlesSiteAnalyzer
    {
        public IEnumerable<RawPage> GetTestPages()
        {
            var enLan = new RawPage("/en".GetTestUri(), "english landing", string.Empty, Enumerable.Empty<Uri>());
            var csLan = new RawPage("/".GetTestUri(), "czech landing", string.Empty, Enumerable.Empty<Uri>());
            return GetTestHierarchy("cs").Concat(GetTestHierarchy("en")).Concat(new[] {enLan, csLan});
        }

        public IEnumerable<RawPage> GetTestHierarchy(string firstSegment)
        {
            for (int i = 0; i < 3; i++)
            {
                yield return new RawPage(string.Format("/{0}/category-{1}", firstSegment, i).GetTestUri(),
                    firstSegment + " category", string.Empty, Enumerable.Empty<Uri>());
                for (int j = 0; j < 5; j++)
                {
                    yield return new RawPage(string.Format("/{0}/category-{1}/article-{2}", firstSegment, i, j).GetTestUri(),
                        firstSegment + " article", string.Empty, Enumerable.Empty<Uri>());
                }
            }
        }

        [Fact(Timeout=1000)]
        //[Fact]
        public void Should_catch_every_page()
        {
            // Arange
            var pages = GetTestPages();
            var analyzer = new ArticlesSiteAnalyzer();
            analyzer.Prepare();

            // Act
            analyzer.Analyze(pages);

            // Assert
            Assert.Equal(pages.Count(), analyzer.MetPages.Count());
            Assert.True(analyzer.MetPages.All(p => p.IsHandled));
        }

        //[Fact(Timeout = 1000)]
        [Fact]
        public void Should_recognize_hierarchy()
        {
            // Arange
            var pages = GetTestPages().ToList();
            var expectedPagesCount = pages.Count + 1 /* Root */ + 2 /* lang-version */ + 2 /* system */ + 2 /* eventsList */  + 2 /* event */;
            var analyzer = new ArticlesSiteAnalyzer();
            analyzer.Prepare();

            // Act
            var pagesTree = analyzer.Analyze(pages);

            // Assert
            Assert.Equal(pages.Count, analyzer.MetPages.Count());
            Assert.True(analyzer.MetPages.All(p => p.IsHandled));
            Assert.Equal(expectedPagesCount, pagesTree.Count());
            Assert.Equal("ROOT", pagesTree.Root.Name);
            Assert.Equal(2, pagesTree.Root.Children.Count);
            Assert.True(pagesTree.Root.Children.Where(p => p.PageType.Name == "article").All(p => p.Children.Count == 5));
        }
   
        //[Fact(Timeout = 1000)]
        [Fact]
        public void Should_create_page_types()
        {
            // Arange
            var pages = GetTestPages().ToList();
            var expectedPagesTypeCount = 7;
            var analyzer = new ArticlesSiteAnalyzer();
            analyzer.Prepare();

            // Act
            var pagesTree = analyzer.Analyze(pages);
            var pageTypes = analyzer.GetPagesTypes();

            // Assert
            Assert.Equal(expectedPagesTypeCount, pageTypes.Count);
            Assert.True(pageTypes.All(pt => pt.Pages.Any()));
        }
    }
}
