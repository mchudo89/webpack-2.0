﻿using MMLib.RapidPrototyping.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.PageTypeAnalysis;
using Webpack.Domain.Model.Entities;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.PageTypeAnalysis
{
    public class When_analyzing_PageType
    {
        [Fact]
        public void Should_find_properties()
        {
            // Arange
            var pageType = PagesGenerator.GetPageType("test");
            var analyzer = new PageTypeAnalyzer();

            // Act
            analyzer.Analyze(pageType);

            // Assert
            Assert.NotEmpty(pageType.Definitions);
            foreach (var page in pageType.Pages)
            {
                var rendered = page.Render();
                Assert.Equal(page.RawPage.TextData, rendered);
                Assert.NotEmpty(page.Properties);
                foreach (var prop in page.Properties)
                {
                    Assert.Contains(prop.Definition, pageType.Definitions);
                }
            }
        }

        [Fact]
        public void Should_create_pages_identical_to_original()
        { 
            // Arange
            var body1 = 
@"<html>
    <head><title>Test Page</title></head>
    <body>
        <div class=""main"">
            <h1>Title - Classicist storytellers grandiloquent scythes</h1>
            <p><strong>Preoccupy felicitous casuals. </strong></p>

        </div>
    </body>
</html>";

            var body2 =
@"<html>
    <head><title>Test Page</title></head>
    <body>
        <div class=""main"">
            <h1>Title - Remotely prokaryotes worsens grids</h1>
            <p><strong>Daddy timeshare lobotomised uncomplimentary. </strong></p>
            <p>Nearest prayerbook drachma pirated. </p>
            <p>Nearest prayerbook drachma pirated. </p>
            <p>Nearest prayerbook drachma pirated. </p>
        </div>
    </body>
</html>";
            var rp1 = new RawPage("/category-1".GetTestUri(), body1, string.Empty, Enumerable.Empty<Uri>());
            var rp2 = new RawPage("/category-2".GetTestUri(), body2, string.Empty, Enumerable.Empty<Uri>());

            var pt = new PageType
            {
                Name = "test"
            };
            pt.Pages.Add(new Page { RawPage = rp1, PageType = pt, Name = "cat1" });
            pt.Pages.Add(new Page { RawPage = rp2, PageType = pt, Name = "cat2" });

            var analyzer = new PageTypeAnalyzer();

            // Act
            analyzer.Analyze(pt);

            // Assert
            Assert.NotEmpty(pt.Definitions);
            foreach (var page in pt.Pages)
            {
                var rendered = page.Render();
                Assert.Equal(page.RawPage.TextData, rendered);
                Assert.NotEmpty(page.Properties);
                foreach (var prop in page.Properties)
                {
                    Assert.Contains(prop.Definition, pt.Definitions);
                }
            }

        }
    }
}
