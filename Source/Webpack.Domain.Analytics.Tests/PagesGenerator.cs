﻿using MMLib.RapidPrototyping.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Entities;

namespace Webpack.Domain.Analytics.Tests
{
    public static class PagesGenerator
    {
        private static readonly Random randomNumbers = new Random();
        private static readonly WordGenerator randomWords = new WordGenerator();

        private const string htmlFormat =
@"<html>
    <head><title>Test Page</title></head>
    <body>
        <div class=""main"">
            <h1>Title - {0}</h1>
            {1}
        </div>
    </body>
</html>";

        public static string RandomWords(int minWords, int maxWords)
        {
            var words = string.Join(" ", randomWords.Next(randomNumbers.Next(minWords, maxWords)));
            var capital = Char.ToUpperInvariant(words[0]);
            return capital.ToString() + words.Substring(1);
        }

        public static string RandomSentences(int min, int max)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < randomNumbers.Next(min, max); i++)
            {
                sb.AppendFormat("{0}. ", RandomWords(2, 5));
            }
            return sb.ToString();
        }

        public static string GetPageBody()
        {
            var title = RandomWords(3, 5);

            var sb = new StringBuilder();
            for (int i = 0; i < randomNumbers.Next(1, 6); i++)
            {
                sb.AppendLine(string.Format(i == 0 ? "<p><strong>{0}</strong></p>" : "            <p>{0}</p>", RandomSentences(1, 2)));
            }
            var text = sb.ToString();

            

            return string.Format(htmlFormat, title, text);
        }


        public static IEnumerable<RawPage> GetRawPages()
        {
            for (int i = 0; i < 3; i++)
            {
                yield return new RawPage(string.Format("/category-{0}", i).GetTestUri(),
                    GetPageBody(), string.Empty, Enumerable.Empty<Uri>());
                for (int j = 0; j < 5; j++)
                {
                    yield return new RawPage(string.Format("/category-{0}/article-{1}", i, j).GetTestUri(),
                        GetPageBody(), string.Empty, Enumerable.Empty<Uri>());
                }
            }
        }

        public static PageType GetPageType(string name)
        {
            var pageType = new PageType
            {
                Name = name
            };

            foreach (var rawPage in GetRawPages())
            {
                pageType.Pages.Add(new Page
                {
                    RawPage = rawPage,
                    Name = rawPage.Url.LastSegment,
                    PageType = pageType
                });
            }
            return pageType;
        }
    }
}
