﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.ModelAnalysis;
using Webpack.Domain.Model.Entities;
using Xunit;

namespace Webpack.Domain.Analytics.Tests.PredicateEngine
{
    public class When_building_predicates
    {
        [Fact(Timeout=1000)]
        public void Should_build_using_conditions()
        {
            // Arange
            var test = "test";

            // Act
            var condition1 = Condition<string>.Create();
            condition1
                .Rule(s => s == test);
            var condition2 = Condition<string>.Create();
            condition2
                .Rule(s => s != test);

            // Assert
            Assert.True(condition1.Evaluate(test));
            Assert.False(condition2.Evaluate(test));
        }

        [Fact(Timeout=1000)]
        public void Should_build_using_multiple_conditions()
        {
            // Arange
            var test = "test";

            // Act
            var condition1 = Condition<string>.Create();
            condition1
                .Rule(s => s == test)
                .Rule(s => s.Length == test.Length);
            var condition2 = Condition<string>.Create();
            condition2
                .Rule(s => s != test)
                .Rule(s => s.Length == test.Length);
            var condition3 = Condition<string>.Create();
            condition3
                .Rule(s => s == test)
                .Rule(s => s.Length != test.Length);
            var condition4 = Condition<string>.Create();
            condition4
                .Rule(s => s != test)
                .Rule(s => s.Length != test.Length);

            // Assert
            Assert.True(condition1.Evaluate(test));
            Assert.False(condition2.Evaluate(test));
            Assert.False(condition3.Evaluate(test));
            Assert.False(condition4.Evaluate(test));
        }

        [Fact(Timeout=1000)]
        public void Should_build_using_multiple_conditions_with_or()
        {
            // Arange
            var test = "test";

            // Act
            var condition1 = Condition<string>.Create();
            condition1.Any(c => c
                .Rule(s => s == test)
                .Rule(s => s.Length == test.Length));
            var condition2 = Condition<string>.Create();
            condition2.Any(c => c
                .Rule(s => s != test)
                .Rule(s => s.Length == test.Length));
            var condition3 = Condition<string>.Create();
            condition3.Any(c => c
                .Rule(s => s == test)
                .Rule(s => s.Length != test.Length));
            var condition4 = Condition<string>.Create();
            condition4.Any(c => c
                .Rule(s => s != test)
                .Rule(s => s.Length != test.Length));

            // Assert
            Assert.True(condition1.Evaluate(test));
            Assert.True(condition2.Evaluate(test));
            Assert.True(condition3.Evaluate(test));
            Assert.False(condition4.Evaluate(test));
        }

        [Fact(Timeout=1000)]
        public void Should_build_using_multiple_conditions_on_members()
        {
            // Arange
            var test = "test";

            // Act
            var condition1 = Condition<string>.Create();
            condition1.Member(s => s.Length, c => c
                .Rule(l => l == test.Length)
                .Rule(l => l < test.Length + 1));
            var condition2 = Condition<string>.Create();
            condition2.Member(s => s.Length, c => c
                .Rule(l => l == test.Length)
                .Rule(l => l >= test.Length + 1));
            var condition3 = Condition<string>.Create();
            condition3.Member(s => s.Length, c => c
                .Rule(l => l != test.Length)
                .Rule(l => l < test.Length + 1));
            var condition4 = Condition<string>.Create();
            condition4.Member(s => s.Length, c => c
                .Rule(l => l != test.Length)
                .Rule(l => l >= test.Length + 1));

            // Assert
            Assert.True(condition1.Evaluate(test));
            Assert.False(condition2.Evaluate(test));
            Assert.False(condition3.Evaluate(test));
            Assert.False(condition4.Evaluate(test));
        }
    }
}