﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webpack.Domain.Analytics.Tests
{
    public static class UriProvider
    {
        public static Uri GetTestUri(this int i)
        {
            return new Uri("http://www.example.org/" + i.ToString());
        }

        public static Uri GetTestUri(this string path)
        {
            return new Uri("http://www.example.org" + path);
        }
    }
}
