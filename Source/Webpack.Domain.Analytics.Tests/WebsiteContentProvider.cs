﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webpack.Domain.Analytics.Tests
{
    public class WebsiteContentProvider : IEnumerable<object[]>
    {
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            var path = "..\\..\\TestData\\OAKK";
            var data = Directory.EnumerateFiles(path, "*.htm", SearchOption.TopDirectoryOnly)
                .Select(file => new Tuple<string, string>("/" + file, File.OpenText(file).ReadToEnd()))
                .ToArray();
            yield return new object[] { data };
        }
    }
}
