﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.Crawler;
using Webpack.Domain.Analytics.DocumentTypeAnalysis;
using Webpack.Domain.Analytics.Extensions;
using Webpack.Domain.Analytics.HierarchyAnalysis;
using Webpack.Domain.Model.Entities;
using Xunit;

namespace Webpack.Domain.IntegrationTests
{
    public class When_analyzing_pages
    {

        [Fact(Skip="Integration")]
        //[Fact]
        public void Should_organize_pages_using_hierarchy_analysis()
        {
            // Arange
            var crawlerCfg = new CrawlerConfiguration
            {
                Uri = new Uri("http://vyzkum.rect.muni.cz/"),
                DepthLimit = 3,
                CountLimit = 100,
                IgnoredPrefixes = new[] { "/umbraco/", "/vyzkum/" }//,
                //IgnoredPaths = new[] { "/cs/systemove-stranky/rss" }
            };
            var crawler = new DefaultCrawler(crawlerCfg, new HtmlAgilityPackLoader());
            var hierarchyAnalyzer = new HierarchyAnalyzer();

            // Act
            var rawPages = crawler.Crawl();
            var pages = rawPages.Select(rp => new Page { RawPage = rp, Name = rp.Path }).ToList();
            var result = hierarchyAnalyzer.DetermineHierarchy(pages);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            var root = result.Root;
            Assert.NotNull(root);
            Assert.NotEmpty(root.Children);
        }

        [Fact(Skip = "Integration")]
        //[Fact]
        public void Should_organize_pages_using_menu_analysis()
        {
            // Arange
            var crawlerCfg = new CrawlerConfiguration
            {
                Uri = new Uri("http://vyzkum.rect.muni.cz/"),
                //DepthLimit = 3,
                CountLimit = 500,
                IgnoredPrefixes = new[] { "/umbraco/", "/vyzkum/" }//,
                //IgnoredPaths = new[] { "/cs/systemove-stranky/rss" }
            };
            var crawler = new DefaultCrawler(crawlerCfg, new HtmlAgilityPackLoader());
            var comparer = new HtmlNodeEqualityComparer(new HtmlAttributeEqualityComparer());
            var menuHierarchyFinder = new MenuHierarchyFinder(new NodePositionEqualityComparer());

            var pageComparingFactory = new PageComparingFactory(comparer);
            var skeletonExtractor = pageComparingFactory.SkeletonExtractor;
            var propertyFactory = pageComparingFactory.PropertyFactory;
            var builder = pageComparingFactory.Builder;

            var menuAnalyzer = new MenuAnalyzer(propertyFactory, "//*[@id=\"h-menu\"]", "//*[@id=\"l-menu\"]");

            // Act
            var rawPages = crawler.Crawl();
            var pages = rawPages.Select(rp => new Page { RawPage = rp, Name = rp.Path }).ToList();
            pages.Accept(skeletonExtractor);
            var skeleton = skeletonExtractor.Result;
            menuAnalyzer.IdentifyMenus(skeleton);
            var menuPropertyNames = menuAnalyzer.MenuPropertyNames;
            var propertyIdentifier = new PropertyIdentifier(skeleton, builder);
            pages.Accept(propertyIdentifier);

            var menuHierarchyAnalyzer = new MenuHierarchyAnalyzer(menuHierarchyFinder, menuPropertyNames);

            var result = menuHierarchyAnalyzer.DetermineHierarchy(pages);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            var root = result.Root;
            Assert.NotNull(root);
            Assert.NotEmpty(root.Children);
        }
    }
}
