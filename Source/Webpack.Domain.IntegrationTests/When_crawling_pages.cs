﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Analytics.Crawler;
using Webpack.Domain.Analytics.Crawler.Handlers;
using Xunit;

namespace Webpack.Domain.IntegrationTests
{
    public class When_crawling_pages
    {
        [Fact]
        public void Should_return_also_resources()
        {
            // Arange
            var crawlerCfg = new CrawlerConfiguration
            {
                Uri = new Uri("http://vyzkum.rect.muni.cz/"),
                DepthLimit = 3,
                CountLimit = 500,
                IgnoredPrefixes = new[] { "/umbraco/", "/vyzkum/" }//,
                //IgnoredPaths = new[] { "/cs/systemove-stranky/rss" }
            };
            var imageHandler = new ImageHandler(Path.GetTempPath(), null);
            var scriptHandler = new ScriptHandler(Path.GetTempPath(), imageHandler);
            var loader = new HtmlAgilityPackLoader(scriptHandler);
            var crawler = new DefaultCrawler(crawlerCfg, loader);

            // Act
            var rawPages = crawler.Crawl();
            var resources = loader.Resources;

            // Assert
            Assert.True(rawPages.Any());
            Assert.True(resources.Any(rp => rp.Url.Path.Contains(".axd")));
            Assert.True(resources.Any(rp => rp.Url.Path.Contains(".gif")));
        }
    }
}
