﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webpack.Domain.Model.Entities;
using Webpack.Domain.Model.Logic;
using Webpack.Domain.Model.Tests;
using Xunit;

namespace Webpack.Domain.Model.Tests
{
    public class When_initializing_PagesTree
    {
        [Fact(Timeout=1000)]
        public void Should_throw_if_root_null()
        {
            // Arange + Act + Assert
            Assert.Throws<ArgumentNullException>(() => new PagesTree(null));
        }

        [Fact(Timeout=1000)]
        public void Should_create_childless_root()
        {
            // Arange + Act
            var pagesTree = new PagesTree();

            // Assert
            Assert.NotNull(pagesTree.Root);
            Assert.Empty(pagesTree.Root.Children);
            Assert.Empty(pagesTree.Root.Name);
        }

        [Fact(Timeout=1000)]
        public void Should_create_childless_root_when_passed_no_pages()
        {
            // Arange
            var pages = Enumerable.Empty<Page>();
            var urlStructure = UrlNode.CreateRoot();
            var unmapped = new List<Page>();

            // Act
            var pagesTree = new PagesTree(urlStructure, pages, unmapped);

            // Assert
            Assert.NotNull(pagesTree.Root);
            Assert.Empty(pagesTree.Root.Children);
            Assert.Empty(pagesTree.Root.Name);
            Assert.Empty(unmapped);
        }

        [Fact(Timeout=1000)]
        public void Should_create_root_with_one_child_when_passed_one_mapped_pages()
        {
            // Arange
            var page = new Page { RawPage = new RawPage(UriProvider.GetTestUri(0), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var pages = new[] { page };
            var urlStructure = UrlNode.CreateRoot();
            urlStructure.AppendChild(UriProvider.GetTestUri(0).PathAndQuery);
            var unmapped = new List<Page>();

            // Act
            var pagesTree = new PagesTree(urlStructure, pages, unmapped);

            // Assert
            Assert.NotNull(pagesTree.Root);
            Assert.Single(pagesTree.Root.Children);
            Assert.Equal(UriProvider.GetTestUri(0).PathAndQuery, pagesTree.Root.Children.Single().RawPage.Path);
            Assert.Empty(pagesTree.Root.Children.Single().Children);
            Assert.Empty(unmapped);
        }

        [Fact(Timeout=1000)]
        public void Should_create_root_with_two_children_when_passed_two_mapped_pages()
        {
            // Arange
            var page1 = new Page { RawPage = new RawPage(UriProvider.GetTestUri(1), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var page2 = new Page { RawPage = new RawPage(UriProvider.GetTestUri(2), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var pages = new[] { page1, page2 };
            var urlStructure = UrlNode.CreateRoot();
            urlStructure.AppendChild(UriProvider.GetTestUri(1).PathAndQuery);
            urlStructure.AppendChild(UriProvider.GetTestUri(2).PathAndQuery);
            var unmapped = new List<Page>();

            // Act
            var pagesTree = new PagesTree(urlStructure, pages, unmapped);

            // Assert
            Assert.NotNull(pagesTree.Root);
            Assert.Equal(2, pagesTree.Root.Children.Count);
            Assert.Contains(UriProvider.GetTestUri(1).PathAndQuery, pagesTree.Root.Children.Select(p => p.RawPage.Path));
            Assert.Contains(UriProvider.GetTestUri(2).PathAndQuery, pagesTree.Root.Children.Select(p => p.RawPage.Path));
            Assert.Empty(pagesTree.Root.Children.SelectMany(p => p.Children));
            Assert.Empty(unmapped);
        }

        [Fact(Timeout=1000)]
        public void Should_create_root_with_one_children_when_passed_one_page_with_one_child()
        {
            // Arange
            var page1 = new Page { RawPage = new RawPage(UriProvider.GetTestUri(1), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var page2 = new Page { RawPage = new RawPage(UriProvider.GetTestUri(2), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var pages = new[] { page1, page2 };
            var urlStructure = UrlNode.CreateRoot();
            urlStructure.AppendChild(UriProvider.GetTestUri(1).PathAndQuery).AppendChild(UriProvider.GetTestUri(2).PathAndQuery);
            var unmapped = new List<Page>();

            // Act
            var pagesTree = new PagesTree(urlStructure, pages, unmapped);

            // Assert
            Assert.NotNull(pagesTree.Root);
            Assert.Single(pagesTree.Root.Children);
            Assert.Equal(UriProvider.GetTestUri(1).PathAndQuery, pagesTree.Root.Children.Single().RawPage.Path);
            Assert.Single(pagesTree.Root.Children.Single().Children);
            Assert.Equal(UriProvider.GetTestUri(2).PathAndQuery, pagesTree.Root.Children.Single().Children.Single().RawPage.Path);
            Assert.Empty(pagesTree.Root.Children.Single().Children.Single().Children);
            Assert.Empty(unmapped);
        }

        [Fact(Timeout=1000)]
        public void Should_return_unmapped_pages()
        {
            // Arange
            var page1 = new Page { RawPage = new RawPage(UriProvider.GetTestUri(1), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var page2 = new Page { RawPage = new RawPage(UriProvider.GetTestUri(2), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var pages = new[] { page1, page2 };
            var urlStructure = UrlNode.CreateRoot();
            var unmapped = new List<Page>();

            // Act
            var pagesTree = new PagesTree(urlStructure, pages, unmapped);

            // Assert
            Assert.Empty(pagesTree.Root.Children);
            Assert.Equal(2, unmapped.Count());
        }

        [Fact(Timeout=1000)]
        public void Should_map_only_pages_that_can_be_mapped()
        {
            // Arange
            var page1 = new Page { RawPage = new RawPage(1.GetTestUri(), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var page2 = new Page { RawPage = new RawPage(2.GetTestUri(), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var page3 = new Page { RawPage = new RawPage(3.GetTestUri(), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var page4 = new Page { RawPage = new RawPage(4.GetTestUri(), "test-data", string.Empty, Enumerable.Empty<Uri>()) };
            var pages = new[] { page1, page2, page3, page4 };
            var urlStructure = UrlNode.CreateRoot();
            urlStructure.AppendChild(1.GetTestUri().PathAndQuery).AppendChild(2.GetTestUri().PathAndQuery);
            urlStructure.AppendChild(3.GetTestUri().PathAndQuery);
            var unmapped = new List<Page>();

            // Act
            var pagesTree = new PagesTree(urlStructure, pages, unmapped);

            // Assert
            Assert.NotNull(pagesTree.Root);
            Assert.Equal(2, pagesTree.Root.Children.Count);
            var child1 = pagesTree.Root.Children.Single(n => n.RawPage.Path == 1.GetTestUri().PathAndQuery);
            Assert.Single(child1.Children);
            Assert.Equal(2.GetTestUri().PathAndQuery, child1.Children.Single().RawPage.Path);
            var child2 = pagesTree.Root.Children.Single(n => n.RawPage.Path == 3.GetTestUri().PathAndQuery);
            Assert.Empty(child2.Children);
            Assert.Single(unmapped);
            Assert.Equal(4.GetTestUri().PathAndQuery, unmapped.Single().RawPage.Path);
        }
    }
}
  